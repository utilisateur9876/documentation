How to Install phpMyAdmin on Ubuntu 18.04
=========================================

Posted on [April 18, 2019](https://www.liquidweb.com/kb/install-phpmyadmin-ubuntu-18-04/) by [Echo Diaz](https://www.liquidweb.com/kb/author/ediaz/) | Updated: July 23, 2019\
Category: [Tutorials](https://www.liquidweb.com/kb/category/tutorials/) | Tags: [apache](https://www.liquidweb.com/kb/tag/apache/), [apache2](https://www.liquidweb.com/kb/tag/apache2/), [installation](https://www.liquidweb.com/kb/tag/installation/), [phpMyAdmin](https://www.liquidweb.com/kb/tag/phpmyadmin/), [Ubuntu](https://www.liquidweb.com/kb/tag/ubuntu/)

Reading Time: < 1 minute

Working with a database can be intimidating at times, but phpMyAdmin can simplify tasks by providing a control panel to view or edit your MySQL or MariaDB database.  In this quick tutorial, we'll show you how to install phpMyAdmin on an [Ubuntu 18.04](https://www.liquidweb.com/products/vps) server.

**Pre-flight**

-   Logged in as root or a user with **sudo** privileges
-   [MariaDB/MySQL must be installed](https://www.liquidweb.com/kb/install-mariadb-ubuntu-1804/)

**Step 1:** Update the **apt** package tool to ensure we are working with the latest and greatest.

`apt update && upgrade`

**Step 2:** Install phpMyAdmin and PHP extensions for managing non-ASCII string and necessary tools.

`apt install phpmyadmin php-mbstring php-gettext`

During this installation you'll be asked for the webserver selection, we will select Apache2 and select ENTER.

![apt_install1](https://lwstatic-a.akamaihd.net/kb/wp-content/uploads/2019/04/apt_install1.png)

In this step, you have the option for automatic setup or to create the database manually. For us, we will do the automatic installation by pressing ENTER for yes.

![apt_install2](https://lwstatic-a.akamaihd.net/kb/wp-content/uploads/2019/04/apt_install2.png)

At this setup, you'll be asked to set the phpMyAdmin password. Specifically for the phpMyAdmin user, phpmyadmin,  you'll want to save this in a secure spot for later retrieval.

![apt_install3](https://lwstatic-a.akamaihd.net/kb/wp-content/uploads/2019/04/apt_install3.png)

**Step 3:**  Enable PHP extension.

`phpenmod mbstring`

Note

If you're running multiple domains on one server then you'll want to configure your** /etc/apache2/apache2.conf** to enable phpMyAdmin to work.

`vim /etc/apache2/apache2.conf`

**Add:**

`Include /etc/phpmyadmin/apache.conf`

**Step 4:**  Restart the Apache service to recognize the changes made to the system.

`systemctl restart apache2`

**Step 5:** Verify phpMyAdmin installation by going to **http://ip/phpmyadmin** (username phpmyadmin).

![phpmyadmin1](https://lwstatic-a.akamaihd.net/kb/wp-content/uploads/2019/04/phpmyadmin1.png)

Still having issues installing?  Our [Liquid Web servers](https://www.liquidweb.com/products/vps) come with 24/7 technical support. You can open a ticket with us right away at <support@liquidweb.com>, give us a call at 800-580-4985 or, open a [chat with us](https://www.liquidweb.com/kb/top-10-2019-password-security-standards/#) to speak to one of our Level 3 Support Admins or a Solutions Advisor today!