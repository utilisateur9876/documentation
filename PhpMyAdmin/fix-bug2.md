Ce problème vient de PHP 7.2, ta version php est bien celle ci ? Dans ce cas tu dois faire ceci :

-   Rends toi dans le fichier /usr/share/phpmyadmin/libraries/sql.lib.php à l'aide de cette commande : nano /usr/share/phpmyadmin/libraries/sql.lib.php
-   Recherche (count($analyzed_sql_results['select_expr'] == 1) à l'aide des touches CTRL + W
-   Remplace le par ((count($analyzed_sql_results['select_expr']) == 1)
-   Pour sauvegarder tu fais CTRL + X et tu écris ensuite Y pour confirmer
-   Ensuite Rends toi dans le fichier /usr/share/phpmyadmin/libraries/plugin_interface.lib.php à l'aide de cette commande : nano /usr/share/phpmyadmin/libraries/plugin_interface.lib.php
-   Recherche if ($options != null && count($options) > 0) à l'aide des touches CTRL + W
-   Remplace le par if (! is_null($options) && count((array)$options) > 0) {
-   Pour sauvegarder tu fais CTRL + X et tu écris ensuite Y pour confirmer
-   Pour confirmer tes changements redémarre ton serveur web : service apache2 restart ou service nginx restart

Maintenant tout est bon et le problème est corrigé !

Et depuis la version 7.3

il y a que la première ligne à changer

-------------------------------------------------------------------------------------------------------------------------------

-   Rends toi dans le fichier /usr/share/phpmyadmin/libraries/sql.lib.php à l'aide de cette commande : nano /usr/share/phpmyadmin/libraries/sql.lib.php
-   Recherche (count($analyzed_sql_results['select_expr'] == 1) à l'aide des touches CTRL + W
-   Remplace le par ((count($analyzed_sql_results['select_expr']) == 1)
-   Pour sauvegarder tu fais CTRL + X et tu écris ensuite Y pour confirmer

-----------------------------------------------------------------------------------------------------------------------------

-   Pour confirmer tes changements redémarre ton serveur web : service apache2 restart ou service nginx restart