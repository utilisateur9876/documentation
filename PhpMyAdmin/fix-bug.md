Fix Bug Phpmyadmin [plugin_interface.lib.php] + Php7.2 + Ubuntu 16.04
=====================================================================

[![Chaloemphon Thipkasorn](https://miro.medium.com/fit/c/96/96/2*8utfqIR0tF8qZFjUZe-IXQ.jpeg)](https://medium.com/@chaloemphonthipkasorn?source=post_page-----92b287090b01----------------------)

[Chaloemphon Thipkasorn](https://medium.com/@chaloemphonthipkasorn?source=post_page-----92b287090b01----------------------)

Follow

[Apr 27, 2018](https://medium.com/@chaloemphonthipkasorn/%E0%B9%81%E0%B8%81%E0%B9%89-bug-phpmyadmin-php7-2-ubuntu-16-04-92b287090b01?source=post_page-----92b287090b01----------------------) - 1 min read

[](https://medium.com/p/92b287090b01/share/twitter?source=post_actions_header---------------------------)

[](https://medium.com/p/92b287090b01/share/facebook?source=post_actions_header---------------------------)

[](https://medium.com/m/signin?operation=register&redirect=https%3A%2F%2Fmedium.com%2F%40chaloemphonthipkasorn%2F%E0%B9%81%E0%B8%81%E0%B9%89-bug-phpmyadmin-php7-2-ubuntu-16-04-92b287090b01&source=post_actions_header--------------------------bookmark_sidebar-)

> Warning in ./libraries/plugin_interface.lib.php#532
>
> count(): Parameter must be an array or an object that implements Countable

![](https://miro.medium.com/max/60/1*SxvFD0KMAyR3Fbpc-ow5eQ.png?q=20)

![](https://miro.medium.com/max/1268/1*SxvFD0KMAyR3Fbpc-ow5eQ.png)

couse of phpmyadmin's library try to count some parameter. At this line 532, I found this code in this path

$ /usr/share/phpmyadmin/libraries/plugin_interface.lib.php

![](https://miro.medium.com/max/60/1*xTR1npKmfPbhAh571d3YWw.png?q=20)

![](https://miro.medium.com/max/1130/1*xTR1npKmfPbhAh571d3YWw.png)

if ($options != null && count($options) > 0) {

i think in new php version.It can't use count() or sizeof() with un array type. Force parameter to array is easy way to solve this bug, Like This

if ($options != null && count((array)$options) > 0) {

![](https://miro.medium.com/max/60/1*TCU-qBQZ_s3VR5oC763coQ.png?q=20)

![](https://miro.medium.com/max/1360/1*TCU-qBQZ_s3VR5oC763coQ.png)

May this trick help your code.