NTLite - Créer un Windows personnalisé (Partie 2)
=================================================

Par [Jeremy BEUZELIN](https://www.supinfo.com/articles/author/218494-jeremy-beuzelin) Publié le 22/09/2017 à 02:02:58 Noter cet article: 

☆☆☆☆☆

 (0 votes)

*Avis favorable du comité de lecture*

Jeremy BEUZELIN
===============

![](https://www.supinfo.com/SupinfoCommonResources/SUPINFO-PICTURES/News/ContentPicture/icone-page-financement@x2.png)

-   [Introduction](https://www.supinfo.com/articles/single/5559-ntlite-creer-windows-personnalise-partie-2#idm44858325583104)
-   [Création du support Windows personnalisé](https://www.supinfo.com/articles/single/5559-ntlite-creer-windows-personnalise-partie-2#idm44858298910592)
-   [Conclusion](https://www.supinfo.com/articles/single/5559-ntlite-creer-windows-personnalise-partie-2#idm44858298841376)
-   [Webographie](https://www.supinfo.com/articles/single/5559-ntlite-creer-windows-personnalise-partie-2#idm44858298839648)

### Introduction

Dans la partie 1 de l'article « NTLite - Créer un Windows personnalisé », nous avons évoqué les possibles raisons qui peuvent nous confronter à l'installation d'un Windows. Nous avons également parlé des désagréments qui l'accompagnent et du temps que cela pouvait prendre. Je vous ai présenté NTLite un logiciel qui permet justement de gagner du temps en intégrant les mises à jour, les pilotes et certains composants directement dans un support d'installation Windows au préalable. Cet article a été scindé en plusieurs parties pour une question de lisibilité et de compréhension. Dans cette partie 2, je vais vous accompagner dans la création du support Windows que l'on va personnaliser.

![](https://www.supinfo.com/articles/resources/218494/5559/0.png)

### Création du support Windows personnalisé

Tout d'abord vous devez disposer d'un fichier ISO Windows ou alors du CD d'installation. Dans le cas d'un fichier ISO Windows 7 par exemple, il vous tout faut d'abord extraire les fichiers dans un dossier.

Pour ce faire, Il faut que vous disposiez soit du logiciel 7-Zip ou soit de Winrar pour effectuer cette opération. Vous pouvez également monter sur un lecteur CD virtuel l'ISO avec Deamon Tools pour ensuite copier les fichiers dans un dossier en local. Tous ces logiciels sont disponibles gratuitement sur internet.

Je suis parti d'un fichier ISO pour l'extraire dans un dossier.

![](https://www.supinfo.com/articles/resources/218494/5559/1.png)

Ensuite, il faut que vous lanciez NTLite, puis tout en haut à droite faites « Ajouter => Dossier de l'image ». Dirigez-vous vers le dossier qui contient le Windows.

![](https://www.supinfo.com/articles/resources/218494/5559/2.png)

Maintenant, une liste dans « Historique de l'image » apparaît. Faites un clic droit sur la version qui nous intéresse, par exemple Windows 7 Professionnel et selectionnez charger.

![](https://www.supinfo.com/articles/resources/218494/5559/3.png)

Attendez que le logiciel charge complètement le Windows pour passer à la suite.

![](https://www.supinfo.com/articles/resources/218494/5559/4.png)

Une fois chargé vous allez voir apparaître des options à gauche.Votre version sélectionnée apparaît à côté d'une pastille verte dans « Image montée ».

![](https://www.supinfo.com/articles/resources/218494/5559/5.png)

Dans « Composants » nous pouvons supprimer des fonctionnalités de Windows, mais attention à n'utiliser cette option que si vous êtes un utilisateur expérimenté. Dans le cas contraire, je vous déconseille d'y toucher le système d'exploitation peut devenir instable si vous supprimez certaines fonctionnalités ou il peut simplement ne pas démarrer.

![](https://www.supinfo.com/articles/resources/218494/5559/6.png)

Dans l'onglet « Fonctionnalités », nous pouvons activer ou désactiver des fonctions du style Windows Search ou les fonctionnalités multimédias

![](https://www.supinfo.com/articles/resources/218494/5559/7.png)

Dans les « Services » on peut choisir de quelle manière va démarrer certains services de Windows, on peut choisir que Windows Update soit en « manuel » au lieu d'automatique par exemple.

![](https://www.supinfo.com/articles/resources/218494/5559/8.png)

Dans l'onglet « Machine Locale » on peut configurer tout ce qui est au niveau de la machine. On peut désactiver l'écriture d'évènement dans le journal ou alors modifier la taille du fichier d'échange et tout un tas d'autres paramètres de ce type.

![](https://www.supinfo.com/articles/resources/218494/5559/9.png)

Dans l'onglet « Utilisateurs » on peut configurer tout ce qui est en rapport avec les utilisateurs. Nous pouvons choisir d'afficher ou de ne pas afficher certaines icônes sur le bureau ou de désactiver l'horloge en bas à droite, par exemple. Attention les paramètres vont s'appliquer à tous les utilisateurs.

![](https://www.supinfo.com/articles/resources/218494/5559/10.png)

Ce qui nous intéresse particulièrement c'est l'onglet « Mise à jour », il nous permet en effet de les intégrer. Mais, il va falloir d'abord les télécharger grâce au logiciel Windows Update Dowloader.

Pour ce faire, je vous invite à télécharger le logiciel WUD [Cliquez Ici](https://www.supinfo.com/articles/single/5559-ntlite-creer-windows-personnalise-partie-2#). Veuillez prendre la dernière version, puis installez-la.

Le site de WUD (Windows Update Downloader) propose une Update List(ULS), mais elle n'est plus à jour depuis très longtemps. Heureusement, NTLite nous redirige vers un site qui donne justement des listes plus récentes et souvent mises à jour [Cliquez Ici](https://www.supinfo.com/articles/single/5559-ntlite-creer-windows-personnalise-partie-2#).

Au moment où j'écris cet article j'ai téléchargé la liste « Download Windows 7 Updates ULZ File 64-bit (updated August 5th 2017) ». J'aurais donc toutes les mises à jour jusqu'à Aout 2017. Ensuite, il faut que vous exécutiez le fichier « Windows_7_SP1_Updates_x64.ulz » que vous venez de télécharger. Il va vous afficher ce message.

![](https://www.supinfo.com/articles/resources/218494/5559/11.png)

Vous pouvez lancer maintenant le logiciel WUD qui normalement affichera la liste chargée. Je conseille de tout cocher sauf les « Optional Software ». Elles posent certains problèmes lors de l'intégration par NTLite qui affiche des messages d'erreur et rend le support inutilisable. Ne vous inquiétez pas ces mises à jour s'installeront toutes seules par Windows Update.

![](https://www.supinfo.com/articles/resources/218494/5559/12.png)

Le « Download Folder » indique où le dossier des mises à jour se situera.

Maintenant, retournez sur l'interface NTLite et dirigez-vous vers l'onglet « Mise à jour » qui va nous permettre d'intégrer ces mises à jour fraîchement téléchargées.

En haut à gauche faites « Ajouter => Dossier de mise à jour » et sélectionnez le dossier.

![](https://www.supinfo.com/articles/resources/218494/5559/13.png)

La liste à gauche représente les mises à jour que nous allons intégrer. La liste de droite nous montre des mises à jour connues, par exemple celles qui sont déjà intégrées à l'ISO ou qui sont absentes ou désinstallées/désactivées.

![](https://www.supinfo.com/articles/resources/218494/5559/14.png)

Dans l'onglet « Drivers », nous pouvons ajouter des pilotes pour qu'ils soient déjà installés au premier démarrage du Windows. On peut mettre nos pilotes de cartes mères ou de carte graphique si on le désire.

![](https://www.supinfo.com/articles/resources/218494/5559/15.png)

L'onglet « Registre » nous permet de modifier directement les ruches et d'intégrer des fichiers de registres.

![](https://www.supinfo.com/articles/resources/218494/5559/16.png)

Dans « Unattended » nous pouvons directement renseigner la clef de licence, paramétrer les différentes partitions du disque sur lequel Windows sera installé et rejoindre un domaine ou un groupe de travail

![](https://www.supinfo.com/articles/resources/218494/5559/17.png)

Dans la partie « Post-Installation » nous pouvons exécuter des scripts, rejoindre un domaine et ajouter des comptes locaux.

![](https://www.supinfo.com/articles/resources/218494/5559/18.png)

Une fois toutes ces personnalisations faites on peut se diriger vers la catégorie « Appliquer » pour finaliser les paramètres de notre support. Dans le mode enregistrement, on peut choisir de créer un support comme l'original avec toutes les éditions (Home, Professional, Ultimate) ou alors de le réduire à une ou plusieurs l'édition qui nous intéressent. Cela permet d'optimiser la taille du support et la vitesse d'installation du système d'exploitation surtout dans le cas d'un DVD.

Il faut cocher la case « Créer un ISO » ce qui va ouvrir une fenêtre pour nous demander le chemin de direction de stockage du fichier. Avec ce fichier ISO, on pourra ensuite créer une clé USB bootable ou alors un DVD d'installation.

![](https://www.supinfo.com/articles/resources/218494/5559/19.png)

Voici un comparatif du poids du support Windows original à gauche et de notre support Windows customisé à droite. J'ai choisi de laisser toutes les éditions dans ce support, car j'en ai l'utilité. On peut ainsi voir qu'avec les mises à jour il pèse plus lourd que l'original, elles ont donc bien été prises en compte.

![](https://www.supinfo.com/articles/resources/218494/5559/20.png)

Je suis ensuite passé à l'installation de ce support dans le but de le tester et de vous montrer le gain de temps. Il me demande de faire d'abord 7 mises à jour importantes + 2 facultatives et lors du redémarrage, il m'en proposera seulement 5 importantes puis 2 facultatives. Tout ceci pour me fera télécharger un poids total de 500Mo. Cela peut représenter beaucoup, mais en fait c'est parce que ma liste ne disposait pas des mises à jour cumulatives de septembre et de plus une nouvelle version 4.7 de Microsoft.Net Framework est sortie ce mois-ci.

![](https://www.supinfo.com/articles/resources/218494/5559/21.png)

Vous pouvez noter que la mise à jour de « Correctif cumulatif de qualité » de septembre 2017 fait pratiquement 200Mo à elle toute seule. On peut s'amuser à la télécharger directement sur le site de Windows et l'intégrer à notre support.

![](https://www.supinfo.com/articles/resources/218494/5559/22.png)

On peut voir sans compter les « échecs » que j'ai installé uniquement 15 mises à jour alors qu'il en aurait fallut plus de 200 pour un Windows 7 en SP1 provenant du support original.

![](https://www.supinfo.com/articles/resources/218494/5559/23.png)

### Conclusion

Dans la partie 1 de l'article « NTLite - Créer un Windows personnalisé » nous avons évoqué ensemble les différentes motivations qui peuvent nous confronter à l'installation d'un Windows, ainsi que des désagréments qui l'accompagne. J'ai présenté NTLite une solution gratuite qui nous propose de créer des supports de Windows où l'on peut intégrer et configurer tout un tas de composants, paramètres et surtout les mises à jour. Dans la partie 2, je vous ai accompagné dans la création du début à la fin de votre support personnalisé. J'ai pu démontrer à la fin de cet article le gain de temps obtenu grâce à l'installation de seulement 15 mises à jour au lieu de plusieurs centaines.

### Webographie

NTLite site officiel - https://www.ntlite.com/

Windows Update Downloader (WUD) - http://www.windowsupdatesdownloader.com/

Raymond.cc - https://www.raymond.cc/blog/create-an-integrated-up-to-date-windows-7-install-disc/