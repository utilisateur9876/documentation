Déploiement de Windows 10 à grande échelle -- MDT & WDS
======================================================

PUBLIÉ [5 FÉVRIER 2019](https://www.barrekevin.com/2019/02/05/deployment-de-windows-10-mdt-wds/) [KÉVIN BARRÉ](https://www.barrekevin.com/author/barrek/)

![](https://www.barrekevin.com/wp-content/uploads/2019/02/WINDOWS_10_DEPLOYMENT_WITH_MDT.png)

Présentation de MDT

Microsoft Deployment Toolkit (MDT) est un ensemble d'outils qui permettent de créer et de déployer des systèmes d'exploitation personnalisés. Il s'agit d'une solution gratuite de chez Microsoft L'utilité de créer un système d'exploitation personnalisé est de pouvoir déployer sur tous les postes clients d'une même entreprise une seule et même version d'OS contenant déjà tous les paramètres et les applications nécessaires. Vous avez la possibilité de déployé votre système d'exploitation sur vos clients via le protocole PXe ou via une image ISO créer dans MDT.

Présentation de l'environnement

L'environnement sur lequel va s'appuiera le serveur SRV-DEP est un Windows 2012 Server R2 avec la configuration matériel suivante :

-   RAM : 4 Go
-   CPU : 4
-   HDD : 60 Go
-   Host-Only

Une machine SRV-BALO2 est « également présente sous Windows 2012 R2, hébergeant les services DNS et Active Directory.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/infra_MDT-1024x386.png)

Infrastructure de déploiement

Les services installés seront les suivants :

-   ADK
-   MDT
-   WDS
-   DHCP

Informations et bonnes pratiques
--------------------------------

Configuration de base du serveur

### 1.Création d'une partition pour le DeploymentShare :

Le DeploymentShare de MDT va être stocké sur une partition différente de C : Je vais donc créer une partition E : de 30 Go en NTFS que je vais nommer DATA

![](https://www.barrekevin.com/wp-content/uploads/2019/02/data-2.png)

### 2\. Configuration réseau de la machine :

Dans une interface PowerShell, je vais exécuter la commande :

Get-Netadapter afin d'identifier le numéro d'index de la carte réseau (prefixlenght)

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Get-Netadapter.png)

Toujours dans la console PowerShell je peux configurer les paramètres IP en fonction de votre prefixlenght .

New-NetIPAddress --InterfaceIndex 12 --IPAddress 192.168.1.203 --PrefixLength 24 --DefaultGateway 192.168.1.254

La configuration IP peut être vérifier avec la commande :

Get-NetipConfiguration

### 3\. Nommage du serveur

Le nommage du serveur peut être effectuer aussi en PowerShell en tant qu'administrateur :

Rename-Computer -NewName "SRV-DEP " -force -restart

[system.environement] ::MachineName

### 4\. Jonction au domaine

Le serveur SRV-DEP fait partie du domaine dep.local je procède donc à l'ajoute du serveur.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/netjoin.png)

Configuration et installation du rôle DHCP

### 5\. Ajour du rôle :

Pour ajouter un rôle sur Windows Server 2012, rendez-vous dans le Gestionnaire de serveur puis dans Gérer, Ajouter des rôles et fonctionnalités.  Je clique sur Suivant deux fois, le choisi de serveur de destination, de mon coté il y en à qu'un seul. Puis dans rôle de serveur je vais cocher Serveur DHCP et je vais Suivant et Installer.

### 6\. Configuration d'une nouvelle étendue DHCP

Je me rends dans la configuration du serveur DHCP affin de créer un pool d'adresses destiné à servir les client PXe en IP.

Je fais dérouler les objets à gauche et je fais une clique droite sur IPv4 puis sur Nouvelle étendue, Suivant et je donne à nom à cette étendue.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/new_etendue_DHCP.png)

Je choisis la plage d'adresse que le serveur DHCP va pouvoir distribuer tout comme le masque de sous réseau.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/plage_ip.png)

Vous pouvez ajouter des adresses qui serons exclue de l'étendue.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/ip_exclue_DHCP.png)

La durée du bail par défaut est configurée sur 8 jours.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/dur%C3%A9e_bail_DHCP.png)

A la question suivante cocher oui et faites Suivant

![](https://www.barrekevin.com/wp-content/uploads/2019/02/config_option-DHCP.png)

Vous pouvez configurer la ou les passerelles par défaut.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/gateway_DHCP.png)

Suivant, et vous pouvez entrer l'adresse IP des serveur DNS que vous voulez distribuer par DHCP.

Une fois terminée activer automatique l'étendue créer précédemment puis Suivant et Terminer.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/activer_%C3%A9tendue_DHCP.png)

Rendez vous maintenant dans Options d'étendue où nous allons rajouter deux options bien utiles pour le boot en PXe.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/option_etendue_DHCP.png)

Faite un clic droit dans Option d'étendue, Configurer les options.

Configurer l'adresse IP ou le nom d'hôte de votre serveur MDT dans l'option 066 Nom d'hôte du serveur de démarrage = nom de la machine ou adresse IP. Faite de même pour l'option suivante :

067 Nom du fichier de démarrage =  Boot\x86\wdsnpb.com

Configuration et installation du rôle WDS

### 7\. Installation de WDS

L'installation du rôle peut se faire via PowerShell en tant qu'Administrateur.

Install-WindowsFeature wds-deployment -includemanagementtools -restart

### 8\. Configuration de WDS

L'outil de configuration WDS s'appelle Services de déploiement Windows.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/logo_WDS.png)

Double clic pour démarrer l'assistant de configuration.

| AFFICHAGE | ACTION |
| Assistant Configuration des services de déploiement Windows | Suivant |
| Intégré à Active Directory | Suivant |
| Chemin d'accès | E:\Remoteinstall Suivant |
| Serveur DHCP proxy | Suivant |
| Cocher : Répondre à tous les ordinateur (connus et inconnus) | Suivant |
| Progression de la tâche | Suivant |

Développer l'arborescence Servers, et clic droit sur SRV-DEP puis dans propriétés, l'onglet Réponse PXE.

Cocher répondre à tous les ordinateur (connus et inconnus)

![](https://www.barrekevin.com/wp-content/uploads/2019/02/strategie_reponse_pxe.png)

Dans l'onglet Démarrer, faites comme ci-dessous :

![](https://www.barrekevin.com/wp-content/uploads/2019/02/D%C3%A9marrer.png)

Dans l'onglet Avancé je vais spécifier mon contrôleur de domaine et le catalogue global.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Avanc%C3%A9.png)

Dans l'onglet DHCP il faut autoriser le serveur WDS à travailler avec le DHCP sur la même machine.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/onglet_DHCP.png)

Micosoft Deployment Toolkit et pré-requis ADK

### 9\. Installation de ADK Windows Assessment and Deployment Kit

Télécharger ADK à ce lien sur le site officiel de Microsoft:

<https://go.microsoft.com/fwlink/?linkid=2026036>

et télécharger WinPe ADK ici :

<https://go.microsoft.com/fwlink/?linkid=2022233>

Exécuter et installer les exécutables de sorte à installer :

-   Outils de déploiement
-   Environnement de pré installation Windows PE
-   Concepteur de configuration

![](https://www.barrekevin.com/wp-content/uploads/2019/02/logo_ADK.png)

### 10\. Installation de MDT

MDT est disponible en téléchargement sur le site de Microsoft.

Téléchargé -- le ici :  <https://www.microsoft.com/en-us/download/details.aspx?id=54259>

Exécuter l'exécutable d'installation :

| Affichage | Action |
| Welcom to the Microsoft Deployment Toolkit... | Next |
| End-user Liscence Agreement | Cocher (I accecpt the terms in the ...) |
| Costum Setup | Next |
| I don't want to join the program at this time | Next |
| Ready to install MDT ... | Install |
| Completed the MDT ... | Finish |

Une fois MDT installé nous allons créer le DeploymentShare.

Pour cela clic droit sur Deployment Shares, New Deployment Share

| Affichage | Action |
| Deployment Sahre path | E:/DeploymentShare Next |
| Full path UNC path : \\SRV-DEP\DeploymentShare$ | Next |
| NDT Deployment Share | Next |
| Laisser par défaut ce qui est déjà coché | Next |
| Détails | Next |
| The process completed successfuly | Finish |

![](https://www.barrekevin.com/wp-content/uploads/2019/02/deploymentShare.png)

### 11\. Ajout de l'OS Windows 10 Entreprise N x64

Je vais utiliser une image de Windows 10 .WIM que j'ai préalablement personnalisé sans Bloatware.

Rendez vous dans Operation System clic droit et Import Operating System.

| Affichage | Action |
| OS Type | Costum image file |
| Source | Browse / Source directory |
| D:\ | Next |
| Destination / Windows 10 Entreprise N x64 | Next |
| Symmary | Next |
| Progress / Opération <<immport>> en cours sur la cible | Finish |

![](https://www.barrekevin.com/wp-content/uploads/2019/02/import_OS.png)

### 12\. Configuration du fichier costumsetting.ini

Cette rubrique vous apprend comment configurer les variables dans Microsoft Deployment Toolkit (MDT) 2013 pour passer et configurer automatiquement des étapes lors du déploiement Windows. Les variables peuvent contrôler aussi bien l'affichage des fenêtres lorsque vous êtes sur l'assistant du déploiement, tout comme la configuration de Windows.

Clic droit sur Deploymentshare -- propriété -- Onglet Rules

Une fois l'onglet Rules configuré pensez à bien enregistrer et appliquer avant de quitter.

| [Settings]\
Priority=Default\
Properties=MyCustomProperty\
[Default]\
;Indique le déploiement d'un OS\
OSInstall=Y

;Ne pas afficher le mot de passe admin S\
kipAdminPassword=YES

;Ne pas afficher le message de Welcome\
SkipBDDDWelcome=NO

;Ne pas afficher la config de la clé de produit\
SkipProductKey=YES

;Activer le monitoring\
EventService=http://srv-dep:9800

;Ne pas afficher la config de l'emplacement des données utilisateurs\
UserDataLocation=NONE

;Ne pas captuer l'OS\
SkipCapture=YES

;Mot de passe admin de la machine local\
AdminPassword==Toto00=

;Passer la liste des appli\
SkipApplications=NO

;Passer la mise à jour les appli\
SkipAppsOnUpgrade=YES

;Passer la configuration de BitLocker\
SkipBitLocker=YES

;Passer le nommage du PC\
SkipComputerName=NO

;Passer le backup du PC\
SkipComputerBackup=YES

;Passer le choix du type de déploiement\
SkipDeploymentType=YES

;Type de deploiement par defaut\
DeploymentType=NEWCOMPUTER

;Passer la jonction au domaine\
SkipDomainMembership=YES

;Passer le sommaire final\
SkipFinalSummary=No

;Passer la sélection des langue local\
SkipLocaleSelection=YES

;Configuration du clavier\
KeyboardLocale=fr-FR

;Configuration de la langue\
UserLocale=fr-FR\
UILanguage=fr-FR

;Passer les package à installer\
SkipPackageDisplay=YES

;Passer le sommaire\
SkipSummary=YES

;Ne pas passer le choix des tache de séquence\
SkipTaskSequence=NO

;Passer le choix de la time zone\
SkipTimeZone=YES

;Choix de la time zone\
TimeZone=105\
TimeZoneName=Romance Standard Time

;Passer les données utilisateur\
SkipUserData=Yes UserDataLocation=AUTO

;Informations de connexion au domaine\
JoinDomain=dep.local\
DomainAdmin=Administrateur\
DomainAdminDomain=dep.local\
DomainAdminPassword==Toto00= |

### 13\. Configuration du bootstrap.ini

Le fichier bootstrap.ini lui contient les informations de connexion au DeploymentShare.

Ce fichier peut être configurer dans DeploymentShare, propriété, Rules Edit Bootstrap.ini

| [Settings]\
Priority=Default\
[Default]\
DeployRoot=\\srv-dep\DeploymentShare\
UserID=deploi_share_user\
UserPassword=Iroise29\
UserDomain=dep.local\
SkipBDDWelcome=YES |

Les modifications du fichier Bootstrap.ini demande une régénération du client LiteTouch.

Completely regenerate the boot images

![](https://www.barrekevin.com/wp-content/uploads/2019/02/update_deploymentshare.png)

### 14\. Configuration de WinPe

La configuration de WinPe ce fait en faisant un clic droit sur notre DeploymentShare  Puis dans propriété, Windows PE.

Ici vous avez la possibilité de générer un fichier ISO de Windows Pe qui peut être utile si votre PC client ne prends pas en charge les fonctionnalité boot PXe.

Si vous pensez n'utiliser que la version x64, vous pouvez désactiver le x86 dans l'onglet General, la mise à jour du DeploymentShare sera donc plus rapide

![](https://www.barrekevin.com/wp-content/uploads/2019/02/platforms-supported.png)

Pour éviter des problèmes lors du choix de l'image de démarrage rendez-vous dans Windows PE, Features et décocher

![](https://www.barrekevin.com/wp-content/uploads/2019/02/MDAC_ADO.png)

Après chaque modification de Windows Pe et du fichier Bootstrap.ini n'oublier pas de mettre à jour le DeploymentShare.  

### 15\. Ajout de l'image WinPe dans WDS

Dans votre console WDS aller sur votre serveur à gauche et rendez-vous dans Images de démarrage. Clic droit Ajouter une image de démarrage puis naviguer dans votre DeploymentShare pour trouver le fichier LiteTouchPE_x64.wim

Moi il se trouve dans E:\DeploymentShare\Boot\LiteTouchPE_x64.wim

Je clic sur Suivant 3 fois et puis Terminer.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/fichier_image.png)

### 16\. Gestion des drivers Windows 10 -- Windows Pe

Pour mes tests j'utilise un PC portable HP 820 G3.

J'ai donc téléchargé les pilotes pour Windows 10 : <http://ftp.hp.com/pub/caps-softpaq/cmit/HP_Driverpack_Matrix_x64.html>

![](https://www.barrekevin.com/wp-content/uploads/2019/02/HP-820-G3.jpg)

Et également les pilotes utilisables pour le client Win Pe : <http://ftp.hp.com/pub/caps-softpaq/cmit/HP_WinPE_DriverPack.html>

Il s'agit d'un package de pilote sous forme d'un .exe qui une fois exécuter me permet de récupérer une arborescence de driver comme ceci :

![](https://www.barrekevin.com/wp-content/uploads/2019/02/pilote_HP.png)

Dans la console MDT rendez-vous dans Out-of-Box Drivers clic droit et faite New Folder.

Je vous conseille de créer une arborescence comme suivant afin de bien organisé vos pilotes, moi je les classe par marque et par model.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Hp_arbo.png)

Maintenant il est temps d'ajouter le driver, commençons par les pilotes Windows 10, je me rends donc dans le dossier Win10 HP 820 G3 clic droit sur Import Drivers puis je vais chercher le dossier contenant mes driver Windows 10 que j'ai extrais auparavant.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Specift_Directory.png)

Je clic sur Next 2 fois, puis Finish.

Je vais faire de même pour les drivers Windows Pe

Ce n'est pas fini, rendez-vous dans Advenced Configurations, Selection profiles.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/advenced_config.png)

Clic droit, New Selection Profile je donne un nom à ce profile, WinPe820G3 (les driver Winpe pour le PC portable)

![](https://www.barrekevin.com/wp-content/uploads/2019/02/selectionprofilname.png)

Next, je vais maintenant choisis le dossier accoisé à ce nouveau profile.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Outofboxdrivers.png)

Next 2 fois et Finish.

Je créer également des profile pour les driver Windows 10.

### 17\. Ajout des drivers dans l'image Windows Pe

Pour l'instant les driver Windows Pe sont seulement dans notre DeploymentShare mais il va falloir les intégrer dans l'image WinPe. Pour cela n'est pas compliqué : clic droit sur le DeploymentShare, Propriété, Windows PE, dans l'onglet Drivers and Patches je vais venir choisir le profile le driver WinPe.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Driver-Injection.png)

Je fais Appliquer, OK puis je régénéré le DeploymentShare .

### 18\. Ajout des applications

Lors du déploiement de Windows, MDT est capable d'installer des applications automatiquement de manière silencieuse.

Je vais installer VLC, Gimp, PDF Creator. Je me rends donc sur les sites officiels des applications pour y télécharger les dernières versions stables au format .msi de préférence.

Rendez-vous sur MDT dans Application , mois j'ai créer un nouveau dossier nommé Application de base clic droit sur mon dossier Application de base New Application.

| Affichage | Action |
| Specity the type of application to add | (Cocher) Application with source files Next |
| Specity the dedails for the this applications | Ici vous pouvez indiquer des information (Nom de l'appli, version, éditeur, langue) Next |
| Source directory | Indiquer le répertoire contenant vos msi d'installation Next |
| Specity the name of the directory that should be created | Entrer le nom de dossier pour cette application Next |
| Specify the quiet install command... | Entrer la ligne de commande qui va exécuter l'installation start /wait msiexec /i vlc-3.0.6-win64.msi /qn Next |

Next deux fois et l'application et ajouter au DeploymentShare.

Vous trouverez sur ce site web : <http://www.get-itsolutions.com/applications-silent-install-complete-list/>

Les commandes à utilisés pour l'installation des softwares les plus courant.

### 19\. Création de la séquence de tâche

Maintenant tous les éléments sont prêts pour créer une séquence de tâche de déploiement.

Rendez-vous dans MDT,  Task Sequances clic droit, New Task Sequence

![](https://www.barrekevin.com/wp-content/uploads/2019/02/newts.png)

Nommer votre séquence de tâche en fonction de ce que vous voulez faire. Je fais Next, je choisis Standad Client Task Sequences Next et je vais choisis mon OS Windows 10 .

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Windows10-Entreprise-N-x64.png)

Je ne spécifie pas de clé de produit, je peux choisis le nom de l'administrateur local et lui indiquer un mot de passe.

Next deux fois et Finish.

La séquence de tâche est créée mais pas configuré. Double clic sur votre Task Sequence

Rendez-vous dans l'onglet Task Sequence.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/TS-Pr%C3%A9sinstall.png)

Les HP 820 G3 dispose d'un SSD de 256 Go, je vais donc configurer la séquence de tâche afin que le disque soit partitionné de la façon suivante : 

-   Une partition OS C : de 120 Go
-   Une partition Donnes D : avec le reste d'espace disponible

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Format-partition.png)

Je vais supprimer les volumes existant s'il y en a et en créer de nouveaux.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Partition-OS-1.png)

Aller dans Preinstall, Inject Drivers et choisissez le profile de driver Windows 10 à importer.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Choose-a-selection-profile.png)

Dans State Restore, Install Applications vous avez la possibilité d'indiquer la ou les applications à installer.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Install-a-single-appli.png)

### 20\. Partage du DeploymentShare avec un utilisateur du domaine

Le dossier DeploymentShare sur mon disque E : doit être partager afin qu'il puisse être accessible lors de la séquence de tâche.

Comme spécifier dans le fichier bootstrap.ini, il s'agis de l'utilisateur deploi_share_user du domaine test.local

| DeployRoot=\\srv-dep\DeploymentShare\
UserID=deploi_share_user\
UserPassword=Iroise29\
UserDomain=test.local |

Je vais donc créer cet utilisateur sur mon contrôleur de domaine.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/deploi-user-share.png)

Maintenant je peux accorder les droits sur le DeploymentShare.

Rendez-vous sur E:,  clic droit sur le DeploymentShare, Propriété dans l'onglet Partage, cliquer sur Partager et la je vais rechercher mon utilisateur créer précédemment et je vais l'ajouter avec le niveau d'autorisation lecture/écriture.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Droit-deploi-share-user.png)

Déploiement de Windows 10

Mon serveur de déploiement SRV-DEP est maintenant opérationnel, il ne me reste plus qu'à essayer tout ça. 

Pour ce faire je créer donc une nouvelle machine virtuelle :

-   HDD : 120 Go
-   RAM : 4 Go
-   CPU : 2
-   Network : Host-Only

![](https://www.barrekevin.com/wp-content/uploads/2019/02/info-VM.png)

Au démarrage de la VM, comme il n'y a pas d'OS présélectionner, le boot via PXe commence. La VM charge le fichier de démarrage LiteTouchPe générer par MDT.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Loadding-file.png)

Après le chargement, l'écran d'accueil de MDT s'affiche.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Welcomm-MDT.png)

Vous pouvez y choisir la langue du clavier, l'adresse IP et également commencer le déploiement.

Je clic donc sur Run the Deployment Wizard...

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Run-the-Deployment-Wizard.png)

Je choisis ma tâche de séquence et je fais Next.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/My-TS.png)

Je choisis le nom du PC 

![](https://www.barrekevin.com/wp-content/uploads/2019/02/Computer-nam.png)

Je choisis également les applications que je veux installer.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/app-to-install.png)

Je fais Next et le déploiement commence.

![](https://www.barrekevin.com/wp-content/uploads/2019/02/run-d%C3%A9ploiement.png)