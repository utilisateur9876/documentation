Le kit de déploiement et d'évaluation de Windows 10 (ADK) est disponible (version finale)
=========================================================================================

![ ](https://microsofttouch.fr/resized-image.ashx/__size/550x0/__key/communityserver-blogs-components-weblogfiles/00-00-00-00-16/4834.data_5F00_migration.jpg)

Microsoft a mis en ligne la version finale du **kit de déploiement et d'évaluation pour Windows 10 (ADK)**, anciennement WAIK. Ce kit vous donne tous les outils nécessaires pour déployer Windows 10 dans votre entreprise :

-   **Windows PE **est le micro environnement Windows utilisé lors du déploiement de système d'exploitation.  Il est basé sur le système d'exploitation Windows 10.
-   Les **outils de déploiement **comme DISM, OSCDIMG, Windows SIM permettent la manipulation des images
-   **(Nouveau) Windows Imaging and Configuration Designer** (ICD) est un nouvel outil permettant de construire des images Windows personnalisées tout comme des packages de provisionnement pour personnaliser l'image Windows par défaut.
-   **Application Compatibility Toolkit (ACT)** est utile dans des scénarios de migration pour identifier les applications pouvant poser problème. Il identifie les problèmes compatibilité applicative sur les postes de travail et donne des solutions (Shims) permettant de résoudre ces problèmes.
-   **User State Migration Tool **est un outil en ligne de commande permettant de migrer l'état des données utilisateurs d'un système d'exploitation à un autre. Il vous permet de migrer les paramétrages systèmes, les données ou encore les paramétrages applicatifs.
-   **Volume Activation Management Tool (VAMT)** est un outil permettant la gestion de l'activation des clients Windows (et applications Office) en déployant des clés et en procédant à l'activation.
-   **Windows Assessment Toolkit **permet de vérifier et valider l'état de conformité d'un ordinateur vis-à-vis du système d'exploitation à déployer. Il vérifiera les aspects matériels et logiciels.
-   **Windows Performance Toolkit (WPT)** permet l'enregistrement des événements système et l'analyse des performances.

Notez que cette version permet de déployer Windows 10, Windows 8.1, Windows 8, or Windows 7 SP1.

Plus d'informations sur : [https://msdn.microsoft.com/en-us/windows/hardware/dn913721(v=vs8.5).aspx](https://msdn.microsoft.com/en-us/windows/hardware/dn913721%28v=vs8.5%29.aspx)

**[Télécharger Windows Assessment and Deployment Kit (Windows ADK)](http://go.microsoft.com/fwlink/p/?LinkId=526740)**