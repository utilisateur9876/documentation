Resetting the WMI Repository

Press Windows+Q and type cmd into the search bar that pops up.

Right-click on Command Prompt and select Run as Administrator

Type net stop winmgmt into the command prompt and press enter.

When prompted if you wish to continue, type Y and press enter.

Type winmgmt /resetrepository into the command prompt and press enter.

Restart your computer to pick up the changes