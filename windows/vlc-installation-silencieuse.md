Source : http://www.get-itsolutions.com/applications-silent-install-complete-list/

Silent install VLC Media Player
===============================

In this tutorial will explain how to silent install VLC Media Player. We will explain both methods for exe and msi file. The script will uninstall older version of VLC. Tested with VLC version 2.2.2 and working perfectly.

VLC Media Player is known as alternative of Windows Media Player and one of the best media players. VLC has also known as free and open source cross-platform. The Player has ability to play most multimedia file including DVDs, Audio CDs, VCDs, and divers streaming protocols.

Silent install guides for other application here: **[Complete List](http://www.get-itsolutions.com/applications-silent-install-complete-list/)**

Silent install VLC Media Player.
--------------------------------

-   VLC exe version:
    -   Download exe.
    -   Create command line.
    -   Execute script.
-   VLC msi version:
    -   Download msi.
    -   Create command line.
    -   Execute script.

### VLC exe version installing silently.

#### **Download exe file.**

To download exe file you can go directly to application webpage : [http://www.videolan.org/vlc/index.html](https://www.videolan.org/vlc/index.html)

![Silent install VLC](http://www.get-itsolutions.com/wp-content/uploads/2016/03/VLC-Media-Player.jpg)

Silent install VLC

Make sure that you downloaded lasts version.

Copy the exe to a folder on your computer. The folder name be like "Silent install VLC"

#### **Create cmd file for silent install.**

Create a text file to the folder created previously. Copy the following commands and save like "Install.cmd".

```
@echo silent install exe version

REM Uninstall old VLC version

if exist "%PROGRAMFILES%\VideoLAN\VLC\uninstall.exe" "%PROGRAMFILES%\VideoLAN\VLC\uninstall.exe" /S

if exist "%PROGRAMFILES(x86)%\VideoLAN\VLC\uninstall.exe" "%PROGRAMFILES% (x86)\VideoLAN\VLC\uninstall.exe" /S

REM VLC Silent install

"%~dp0vlc-2.2.2-win32.exe" /L=1033 /S /NCRC

```

/L=1033 -- Set language to English

/S -- Install silently

/NCRC --  Skip CRC Check

Note: Change the "vlc-2.2.2-win32.exe" with your filezilla name downloaded.

Note: If you doesn't have any other version of filezilla installed, you can delete commands for uninstalling old version.

#### **Execute command.**

Run as Administrator the Install.cmd script and the VLC Media Player exe version will be installed silently.

### **Silent install VLC msi version.**

VLC Media Player do not supply any msi version of application. Anyhow we have founded an good website to get Filezilla MSI. The package tested successfully.

#### **Download msi file.**

Download VLC 2.2.2 msi file to following link: <https://sourceforge.net/projects/vlcplayermsiinstallers/files/v2.2.2/vlcplyer222.zip>

Unzip and copy the msi file to an folder called "VLC msi silent install"

#### **Create cmd file for msi silent install.**

Create a text file to the folder created previously, copy the following commands and save like "Install.cmd":

```
@echo Silent install msi version

REM Uninstall any previous version of VLC

if exist "%PROGRAMFILES%\VideoLAN\VLC\uninstall.exe" "%PROGRAMFILES%\VideoLAN\VLC\uninstall.exe" /S
if exist "%PROGRAMFILES(x86)%\VideoLAN\VLC\uninstall.exe" "%PROGRAMFILES% (x86)\VideoLAN\VLC\uninstall.exe" /S

@Echo VLC silent install msi

start /wait msiexec /i "%~dp0VLC_Player_2.2.2.msi" /qn

```

Note! Change the " VLC_Player_2.2.2.msi " with your msi name file.

#### **Execute command.**

Just Run As administrator the cmd file and filezilla will be installed silently.

If you have any question feel free to ask on the comment section.

**Please rate use if this article was helpful to you**