http://www.get-itsolutions.com/applications-silent-install-complete-list/

Silent Install Google Chrome and Disable Auto Update
====================================================

In this tutorial will explain how to **silent install Google Chrome msi** and **disable auto update**. The command will **uninstall older version of Google Chrome **and install a new version. Also the script will disable auto update of google chrome. This is important if you don't want automatically update. We will use MSI installer to make it much easier.

Many times it is an issue with the Administrators to deploy Google Chrome enterprise wide as there are lots of issues like:\
1) Many people would have already installed various other versions of Chrome either through MSI or through exe.\
2) How to customize Google chrome for enterprise wide deployment.

**Silent Install Google Chrome.**
---------------------------------

1.  Download msi.
2.  Create .cmd file for silent install google chrome.
3.  Create Master Preferences file.
4.  Disable auto update.
5.  Execute silent install google chrome.

Silent install guides for other application here: **[Complete List](http://www.get-itsolutions.com/applications-silent-install-complete-list/)**

### What is the difference between Google Chrome and Google Chrome Enterprise?

Google Chrome Enterprise installs for use for any user on the machine it's installed on. This is ideal for ensuring all potential users have the latest version of Google Chrome whereas the non-Enterprise Google Chrome will only install per user. This could leave you with differing versions of Google Chrome over time if not managed.

### **Download msi for silent install Google Chrome.**

First download the latest version MSI. You can now download the Google Chrome MSI installer direct from Google from the link below:

[http://www.google.co.uk/intl/en/chrome/business/browser/admin/](https://www.google.co.uk/intl/en/chrome/business/browser/admin/)

**<https://www.google.com/chrome/browser/desktop/index.html?msi=true>**

The downloaded msi save to a folder in your computer and rename:

"googlechromestandaloneenterprise.msi**"**

### **Create the .cmd file for silent install google chrome.**

Create a text file called **install.cmd** and copy in the following command lines:

```
@echo install Google Chrome
start /wait msiexec /i "%~dp0%googlechromestandaloneenterprise.msi%" /qn /l*
@echo Set the parameter file
@XCOPY "%~dp0master_preferences.txt" "C:\Program Files\Google\Chrome\Application\" /E /C /Q /R /Y
@XCOPY "%~dp0master_preferences.txt" "C:\Program Files (x86)\Google\Chrome\Application\" /E /C /Q /R /Y
reg add HKLM\Software\Policies\Google\Update /f /v AutoUpdateCheckPeriodMinutes /d 0
sc stop gupdate
sc config gupdate start= disabled
@echo disable auto update
REGEDIT /S "%~dp0%chrome-manual-updates.reg%" /qn

```

### **Create Master Preferences file.**

Create text file with named "master_preferences.txt" and copy the line below:

```
{
"homepage" : "http://www.google.com",
"homepage_is_newtabpage" : false,
"browser" : {
"show_home_button" : true,
"check_default_browser" : false,
"window_placement": {
"bottom": 1000,
"left": 10,
"maximized": false,
"right": 904,
"top": 10,
"work_area_bottom": 1010,
"work_area_left": 0,
"work_area_right": 1680,
"work_area_top": 0
}
},
"bookmark_bar" : {
"show_on_all_tabs" : true
},
"distribution" : {
"skip_first_run_ui" : true,
"show_welcome_page" : false,
"import_search_engine" : false,
"import_history" : false,
"create_all_shortcuts" : true,
"do_not_launch_chrome" : true,
"make_chrome_default" : false
}

```

### **Disable auto update Google Chrome.**

Copy the following command lines in the new file named "chrome-manual-updates.reg":

```

Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Update]
"UpdateDefault"=dword:00000000

[HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Google\Update]
"UpdateDefault"=dword:00000000

[HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Update]
"DisableAutoUpdateChecksCheckboxValue"=dword:00000001

[HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Google\Update]
"DisableAutoUpdateChecksCheckboxValue"=dword:00000001

[HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Google\Update]
"AutoUpdateCheckPeriodMinutes"=dword:00000000

[HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Google\Update]
"AutoUpdateCheckPeriodMinutes"=dword:00000000
```

### **Execute Google Chrome silent installation.**

The folder look like :

![Google chrome silent install](http://www.get-itsolutions.com/wp-content/uploads/2015/06/11-1.jpg)

Google chrome silent install

Just right-click on install.cmd and run as administrator. After few seconds the install will finished.

### 32-bit or 64-bit?

Yes, there is a 64-bit Google Chrome. However, the majority of installs are of the 32-bit (even for 64-bit machines.) If you would like the 64-bit keep in mind that you will also need to make sure that your Java is also 64-bit as well.

### About Google Chrome.

**Google Chrome** is a freeware web browser developed by Google. It used the WebKit layout engine until version 27 and, with the exception of its iOS releases, from version 28 and beyond uses the WebKit forkBlink. It was first released as a beta version for Microsoft Windows on September 2, 2008, and as a stable public release on December 11, 2008. As of October 2015, StatCounter estimates that Google Chrome has a 56% worldwide usage share of web browsers as a desktop browser. It is also the most popular browser for smartphones, and combined across all platforms at about 45%. Its success has led to Google expanding the 'Chrome' brand name on various other products such as the Chromecast.

Deploy Google Chrome.

You can deploy google chrome with sccm 2012.

Follow the link for more informations : [Deploy Google Chrome](http://www.get-itsolutions.com/deploy-google-chrome-package-with-sccm-2012/)

If you have any question about **Google Chrome silent installation**, do feel free to ask in the comment section.