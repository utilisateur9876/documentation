# documentation

Le but est de tenir une documentation à jour et utilisable partout.


curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall && \
  chmod 755 msfinstall && \
  ./msfinstall
  
  
WPScan Install on Ubuntu
WPScan project page

WPScan can test a WordPress installation for security vulnerabilities. The tool is a black box scanner, it allows remote testing of a WordPress installation. Find vulnerable plugins and themes, security configuration issues and attack users by brute forcing passwords.

Installation on Ubuntu Linux is pretty straight forward and you will be up and running in a few minutes. Alternatively you could try using Kali Linux a pre-built penetration testing Linux distribution that comes with WPScan pre-installed.

Install Steps for WPScan on Ubuntu
If you do not have git installed, you will need it. Git is a tool that allows easy access for installation and updates to a code repository.

apt-get install git
Now for a few prerequisites.....

sudo apt-get install libcurl4-openssl-dev libxml2 libxml2-dev libxslt1-dev ruby-dev build-essential libgmp-dev zlib1g-dev
Now to use git you clone the main branch of WPScan code, this will create a folder on your system with the code.

git clone https://github.com/wpscanteam/wpscan.git

cd wpscan

sudo gem install bundler && bundle install --without test development
To launch the wpscan.rb launch it with ruby.

ruby wpscan.rb
_______________________________________________________________
        __          _______   _____                  
        \ \        / /  __ \ / ____|                 
         \ \  /\  / /| |__) | (___   ___  __ _ _ __  
          \ \/  \/ / |  ___/ \___ \ / __|/ _` | '_ \ 
           \  /\  /  | |     ____) | (__| (_| | | | |
            \/  \/   |_|    |_____/ \___|\__,_|_| |_|

        WordPress Security Scanner by the WPScan Team 
                    Version v2.1r2d5a770
     Sponsored by the RandomStorm Open Source Initiative
 @_WPScan_, @ethicalhack3r, @erwan_lr, @gbrindisi, @_FireFart_
_______________________________________________________________


Examples :

-Further help ...
ruby wpscan.rb --help

-Do 'non-intrusive' checks ...
ruby wpscan.rb --url www.example.com

-Do wordlist password brute force on enumerated users using 50 threads ...
ruby wpscan.rb --url www.example.com --wordlist darkc0de.lst --threads 50

-Do wordlist password brute force on the 'admin' username only ...
ruby wpscan.rb --url www.example.com --wordlist darkc0de.lst --username admin

-Enumerate installed plugins ...
ruby wpscan.rb --url www.example.com --enumerate p

-Enumerate installed themes ...
ruby wpscan.rb --url www.example.com --enumerate t

-Enumerate users ...
ruby wpscan.rb --url www.example.com --enumerate u

-Enumerate installed timthumbs ...
ruby wpscan.rb --url www.example.com --enumerate tt

-Use a HTTP proxy ...
ruby wpscan.rb --url www.example.com --proxy 127.0.0.1:8118

-Use a SOCKS5 proxy ... (cURL >= v7.21.7 needed)
ruby wpscan.rb --url www.example.com --proxy socks5://127.0.0.1:9000

-Use custom content directory ...
ruby wpscan.rb -u www.example.com --wp-content-dir custom-content

-Use custom plugins directory ...
ruby wpscan.rb -u www.example.com --wp-plugins-dir wp-content/custom-plugins

-Update ...
ruby wpscan.rb --update

-Debug output ...
ruby wpscan.rb --url www.example.com --debug-output 2>debug.log

See README for further information.

No argument supplied
Like any vulnerability scanner that uses signatures or known issues as part of its detection capability WPScan needs to be updated on a regular basis. Thankfully the WPScan team update it on a regular basis.

Update WPScan
To update simply launch a git pull command from within the installation directory.

cd wpscan
git pull