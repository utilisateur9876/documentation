Configuring Let's Encrypt SSL Cert for Apache on Ubuntu 18.04 / 19.10
=====================================================================

Last updated on February 23rd, 2020

**Let's Encrypt** is a certificate authority that provides free SSL certificates that are just as secure as current paid certificates. In this guide we will configure an SSL certificate for Apache on Ubuntu 18.04 / 19.10.

Prerequisites
-------------

You should be using a non-root user with sudo privileges as explained in [Ubuntu 18.04 / 19.10 Initial Server Setup](https://devanswers.co/ubuntu-18-04-initial-server-setup/).

You should also have **Apache** already installed and serving web pages before continuing with this guide. Please see [Installing Apache on Ubuntu 18.04 / 19.10](https://devanswers.co/installing-apache-ubuntu-18-04-server-virtual-hosts/).

**Cloudflare Users:** Note that you may not need Let's Encrypt and can instead use Cloudflare's own shared Universal SSL certificate and an [Origin CA](https://devanswers.co/configure-cloudflare-origin-ca-apache/). If you want to keep Cloudflare and also use Let's Encrypt, you must [**Pause**](https://support.cloudflare.com/hc/en-us/articles/200169176-How-do-I-temporarily-pause-Cloudflare-) Cloudflare now, otherwise it will interfere with certificate deployment. Once the Let's Encrypt cert is deployed, you must unpause and set SSL to **Full (Strict)** in the Cloudflare crypto settings, otherwise you may get a redirect loop error.

1\. Install Let's Encrypt client (Certbot)
------------------------------------------

Let's begin by updating the package lists and installing software-properties-common. Commands separated by `&&` will run in succession.

```
sudo apt-get update && sudo apt-get install software-properties-common
```

Now add the repositories `universe` and `certbot`.

```
sudo add-apt-repository universe && sudo add-apt-repository ppa:certbot/certbot
```

Press `ENTER` if prompted.

Update the package lists again and install `certbot` for Apache. This is the Let's Encrypt client.

```
sudo apt-get update && sudo apt-get install certbot python3-certbot-apache
```

Press `y` and `ENTER` when prompted to continue.

2\. Get an SSL Certificate
--------------------------

We will now obtain a cert for our test domain **example.com**. Certbot has an Apache plugin, which automates the certificate installation.

```
sudo certbot --apache
```

```
Enter email address (used for urgent renewal and security notices) (Enter 'c' to
cancel):
```

Enter an email address where you can be contacted in case of urgent renewal and security notices.

```
Please read the Terms of Service at
https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf. You must
agree in order to register with the ACME server at
https://acme-v02.api.letsencrypt.org/directory
-------------------------------------------------------------------------------
(A)gree/(C)ancel:
```

Press `a` and `ENTER` to agree to the Terms of Service.

```
Would you be willing to share your email address with the Electronic Frontier
Foundation, a founding partner of the Let's Encrypt project and the non-profit
organization that develops Certbot? We'd like to send you email about EFF and
our work to encrypt the web, protect its users and defend digital rights.
-------------------------------------------------------------------------------
(Y)es/(N)o:
```

Press `n` and `ENTER` to not share your email address with EFF.

```
Which names would you like to activate HTTPS for?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: example.com
2: www.example.com
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate numbers separated by commas and/or spaces, or leave input
blank to select all options shown (Enter 'c' to cancel):
```

If you have multiple domains already configured on your server, you will see a list of them here. In this example, we only have one domain **example.com** and its **www.** prefix.

Select option `1` if you don't want to use the www. prefix in your website address, otherwise select option `2`.

```
Obtaining a new certificate
Performing the following challenges:
http-01 challenge for example.com
Waiting for verification...
Cleaning up challenges
Created an SSL vhost at /etc/apache2/sites-available/example.com-le-ssl.conf
Enabled Apache socache_shmcb module
Enabled Apache ssl module
Deploying Certificate to VirtualHost /etc/apache2/sites-available/example.com-le-ssl.conf
Enabling available site: /etc/apache2/sites-available/example.com-le-ssl.conf

Please choose whether or not to redirect HTTP traffic to HTTPS, removing HTTP access.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: No redirect - Make no further changes to the webserver configuration.
2: Redirect - Make all requests redirect to secure HTTPS access. Choose this for
new sites, or if you're confident your site works on HTTPS. You can undo this
change by editing your web server's configuration.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate number [1-2] then [enter] (press 'c' to cancel):
```

Press `2` and `ENTER` to redirect all traffic to HTTPS.

```
Redirecting vhost in /etc/apache2/sites-enabled/example.com.conf to ssl vhost in /etc/apache2/sites-available/example.com-le-ssl.conf

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Congratulations! You have successfully enabled https://example.com

You should test your configuration at:
https://www.ssllabs.com/ssltest/analyze.html?d=example.com
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
```

You're done!

3\. Test SSL
------------

You can now go to [ssllabs.com/ssltest/](https://www.ssllabs.com/ssltest/) and run an SSL test on your domain.

A successful test should receive an A rating.

![](https://devanswers.co/wp-content/uploads/2018/01/ssl-report-1024x561.png)

4\. Auto Renewal
----------------

As Let's Encrypt certs expire after 90 days, they need to be checked for renewal periodically. Certbot will automatically run twice a day and renew any certificate that is within thirty days of expiration.

To test that this renewal process is working correctly, you can run:

```
sudo certbot renew --dry-run
```

Let me know in the comments if this helped. Follow me on [Twitter](https://twitter.com/DevAnswers), [Facebook](https://www.facebook.com/DevAnswers-326312698016716/) and [YouTube](https://www.youtube.com/channel/UCXvcK0LxaefzAgkUJWqd0FA).

p.s. I increased my AdSense revenue by 68% using AI 🤖. Read my [Ezoic review](https://devanswers.co/ezoic-review-increase-adsense-ad-revenue/) to find out how.
