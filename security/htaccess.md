.htaccess à la racine du site
-----------------------------

Si votre installation s'est bien passée, vous trouverez un fichier `.htaccess` à la racine de votre site. Il contiendra le code suivant :

```
# BEGIN WordPress

RewriteEngine On
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]

# END WordPress
```

Code par défaut du fichier .htaccess de WordPress

[Si vous utilisez WordPress en mode multisite](https://wpmarmite.com/multisite-wordpress/), le code par défaut du fichier `.htaccess` sera différent. Cela ne vous concernera pas dans la majorité des cas.

Maintenant que vous avez localisé ce fichier, vous allez pouvoir enrichir son contenu avec les morceaux de codes ci-dessous pour obtenir des choses bien précises. Cela peut concerner la sécurité, mais aussi d'autres choses.

Veillez à ne pas inclure de code entre les commentaires `# BEGIN WordPress` et `# END WordPress` car il est possible que ce code soit modifié dans certains cas.

**Avertissement** : Faites une sauvegarde de votre fichier `.htaccess` d'origine avant d'effectuer la moindre modification. Vous devez pouvoir revenir en arrière en cas de problème.

### Désactiver l'affichage des répertoires

Par défaut, si vous essayez d'accéder aux répertoires d'un site, le serveur les affichera. La mise en forme ressemblera à ceci :

![Fichiers et répertoires visibles de WordPress](https://media-6d6e.kxcdn.com/wp-content/uploads/2015/10/fichiers-repertoires-visibles-wordpress.png)

Vous vous doutez bien que cela est du pain bénit pour les pirates. Le fait qu'ils puissent voir les fichiers de votre site va les aider à mieux pouvoir l'attaquer. Insérez le code suivant dans votre fichier `.htaccess` pour protéger votre site :

```
# Désactiver l'affichage du contenu des répertoires
Options All -Indexes
```

Il est aussi possible d'utiliser ce code pour empêcher le listage des répertoires :

```
# Alternative pour empêcher le listage des répertoires
IndexIgnore *
```

Besoin d'un hébergeur pour votre site ?

Faites comme WPMarmite, choisissez o2switch. Non seulement **les performances sont au rendez-vous** mais le support est exceptionnel.

[ESSAYEZ O2SWITCH](https://wpmarmite.com/o2switch/)

![](https://media-6d6e.kxcdn.com/wp-content/uploads/2019/09/o2switch-fusee.png)

### Cacher les informations du serveur

Chez certains hébergeurs, les pages affichées peuvent contenir des informations relatives au serveur. Ces informations peuvent donner des informations à d'éventuels assaillants.

Il est donc préférable de les masquer avec le code suivant :

```
# Masquer les informations du serveur
ServerSignature Off
```

### Activer le suivi des liens symboliques

Je dois vous parler chinois mais il est important d'insérer cette ligne de code dans votre fichier `.htaccess` principal.

```
# Activation du suivi des liens symboliques
Options +FollowSymLinks
```

Grâce à cela votre serveur pourra suivre ce que l'on appelle des liens symboliques, c'est-à-dire des raccourcis.

### Mettre votre serveur à l'heure

Cela n'est pas vraiment important mais si votre serveur se trouve à l'étranger, vous pouvez lui indiquer de se caler sur votre fuseau horaire avec cette ligne de code :

```
# Choix du fuseau horaire
SetEnv TZ Europe/Paris
```

### Définir l'encodage des caractères par défaut

Le code suivant permet de définir l'encodage des caractères des fichiers textes et HTML en tant que UTF-8. Sans cela, il y a des risques que les accents soient mal pris en compte.

```
# Encodage par défaut des fichiers textes et HTML
AddDefaultCharset UTF-8
```

### Protéger le fichier wp-config.php

Le fichier de configuration de votre site (wp-config.php) contient les identifiants pour se connecter à la base de données.

C'est le fichier le plus sensible de votre site. Il sera clairement la cible d'éventuels pirates. Il est possible de le protéger en ajoutant ce code au fichier `.htaccess` principal :

```
# Protéger le fichier wp-config.php
<files wp-config.php>
order allow,deny
deny from all
</files>
```

### Protéger le fichier .htaccess lui-même

Tout comme le fichier wp-config.php, le fichier `.htaccess` doit être protégé au maximum. Pour ce faire, insérez ce code :

```
# Protéger les fichiers .htaccess et .htpasswds
<Files ~ "^.*\.([Hh][Tt][AaPp])">
order allow,deny
deny from all
satisfy all
</Files>
```

### Limiter le spam des commentaires

Vous le savez autant que moi si vos avez un blog, [le spam de commentaires est une vraie plaie](https://wpmarmite.com/captcha-wordpress/).

Heureusement, il y a une astuce pour s'en prémunir directement dans le fichier `.htaccess`. Cela n'est pas une solution miracle mais combiné [avec le plugin Akismet](https://wpmarmite.com/akismet-wordpress/), la majorité des spams devrait être filtrée.

```
# Éviter le spam de commentaires
<IfModule mod_rewrite.c>
RewriteCond %{REQUEST_METHOD} POST
RewriteCond %{REQUEST_URI} .wp-comments-post\.php*
RewriteCond %{HTTP_REFERER} !.monsite.com.* [OR]
RewriteCond %{HTTP_USER_AGENT} ^$
RewriteRule (.*) ^http://%{REMOTE_ADDR}/$ [R=301,L]
</IfModule>
```

N'oubliez pas de remplacer monsite.com par votre nom de domaine.

### Éviter que l'on découvre l'identifiant d'un auteur

Même si vous utilisez un identifiant utilisateur complexe, il peut cependant être découvert.

Bien sûr, j'imagine que vous ne l'affichez pas déjà publiquement avec votre thème (ça peut arriver).

Essayez de taper `monsite.com/?author=x` en remplaçant x par 1 pour l'administrateur ou l'ID d'un de vos auteurs. Si vous n'êtes pas protégé, vous serez redirigé vers une page du type `monsite.com/author/idenfiant_auteur`.

Voilà comment on trouve un identifiant en 2 secondes. À partir de là, il ne reste plus qu'à tenter de deviner votre mot de passe.

Pour vous protéger de cette technique, utilisez le code suivant :

```
# Éviter que l'on découvre l'identifiant d'un auteur
# Merci à Jean-Michel Silone du groupe Facebook WP-Secure https://www.facebook.com/groups/wp.securite/
<IfModule mod_rewrite.c>
RewriteCond %{QUERY_STRING} ^author=([0-9]*)
RewriteRule .* - [F]
</IfModule>
```

Merci à Jean-Michel [du groupe Facebook WP-Secure](https://www.facebook.com/groups/wp.securite/) pour l'astuce 🙂

### Désactiver le hotlinking de vos images

Un nouvel anglicisme fait son apparition sur WPMarmite. Rassurez-vous, je vous explique tout.

En fait, une fois que vous aurez ajouté des images sur votre site (par exemple dans un article), n'importe quelle personne pourra copier l'adresse URL d'une de vos images, et l'afficher sur son site.

On pourrait se dire que cela n'est pas si grave mais, si pour une raison X ou Y un site très suivi reprend votre image et l'affiche sur une de ses pages, des requêtes seront effectuées au niveau de votre serveur.

**Le hotlinking est en réalité un vol de bande passante**. Si votre site est installé sur un petit serveur mutualisé, votre hébergeur risque de ne pas apprécier car les ressources sont limitées.

Pour éviter le problème, insérez et personnalisez ce code dans votre fichier `.htaccess` :

```
# Désactiver le hotlinking de vos images
RewriteEngine On
RewriteCond %{HTTP_REFERER} !^$
RewriteCond %{HTTP_REFERER} !^http(s)?://(www\.)?monsite.com [NC]
RewriteRule \.(jpg|jpeg|png|gif)$ http://fakeimg.pl/400x200/?text=Pas_touche_aux_images [NC,R,L]
```

Remplacez monsite.com par votre nom de domaine

Pour autoriser certains sites à afficher vos images, utilisez le code suivant :

```
# Désactiver le hotlinking de vos images
RewriteEngine On
RewriteCond %{HTTP_REFERER} !^$
RewriteCond %{HTTP_REFERER} !^http(s)?://(www\.)?monsite.com [NC]
RewriteCond %{HTTP_REFERER} !^http(s)?://(www\.)?monsite2.com [NC]
RewriteCond %{HTTP_REFERER} !^http(s)?://(www\.)?monsite3.com [NC]
RewriteRule \.(jpg|jpeg|png|gif)$ http://fakeimg.pl/400x200/?text=Pas_touche_aux_images [NC,R,L]
```

Remplacez monsite.com, monsite2.com et monsite3.com par les domaines de votre choix

Vous pouvez aussi personnaliser l'image qui s'affichera à la place de l'image demandée. J'ai ajouté quelque chose de simple, mais vous pouvez être plus taquin.

### Bannir des adresses IP

Si vous avez remarqué que certaines IP tentaient de se connecter un peu trop souvent à l'administration de votre site (par exemple avec [le plugin Login Lockdown](https://wpmarmite.com/login-lockdown/)), vous pouvez vous en débarrasser en bannissant leur adresse IP.

Vous avez aussi la possibilité de récupérer les adresses IP des spammeurs de commentaires pour les bannir de votre site.

Cette solution n'est pas définitive car votre assaillant pourra changer d'adresse IP, mais cela pourra fonctionner pour les personnes les moins douées.

```
# Bannir une adresse IP
<Limit GET POST>
order allow,deny
deny from xxx.xxx.xxx.xxx
allow from all
</Limit>
```

Remplacez xxx.xxx.xxx.xxx par l'adresse IP à bannir

### Bloquer les visiteurs provenant de certains sites

Si vous vous rendez compte qu'un site non conforme a fait un lien pointant vers vous et que vous ne voulez pas que les visiteurs de ce site aient accès à votre site, utilisez ce code :

```
# Empêcher les visiteurs de ces sites d'accéder au votre
<IfModule mod_rewrite.c>
 RewriteEngine on
 RewriteCond %{HTTP_REFERER} monsite1.com [NC,OR]
 RewriteCond %{HTTP_REFERER} monsite2.com [NC,OR]
 RewriteRule .* - [F]
</ifModule>
```

Remplacez monsite1.com et monsite2.com par les sites de votre choix

### Rediriger les visiteurs provenant d'un site vers un autre

Pour aller plus loin que l'astuce précédente, vous pouvez renvoyer les visiteurs provenant de certains sites vers un autre site.

Autant vous dire qu'il y a de quoi bien rigoler. Voici le code à employer :

```
# Rediriger les visiteurs venant site vers un autre
RewriteEngine on
RewriteCond %{HTTP_REFERER} sitesource\.com/
RewriteRule ^(.*)$ http://www.sitedestination.com [R=301,L]
```

Remplacez les sites source et destination par ceux de votre choix

### Créer des redirections

[Le fichier `.htaccess` permet de faire des redirections](https://wpmarmite.com/redirection-wordpress/) (rediriger une URL A vers une URL B).

Cela est bien pratique pour rediriger quelques pages, mais si vous souhaitez créer beaucoup de redirections, je vous conseille le plugin WordPress Redirection.

Voici tout de même comment créer des redirections dans le fichier `.htaccess` :

```
# Redirection d'une page quelconque
Redirect 301 /anciennepage/ http://www.monsite.com/nouvellepage

# Redirection d'une nouvelle catégorie (avec renommage de category en categorie)
Redirect 301 /category/technologie/ http://www.monsite.com/categorie/techno/
```

### Rediriger l'adresse sans www vers celle avec www

Quand on met en place un site, une des actions à accomplir en priorité est de rediriger le site sans les www vers la version dotée des www (ou l'inverse).

Si vous faites le test la prochaine fois que vous créerez un site, vous constaterez que les deux adresses ne renvoient pas forcément vers votre site.

Dans certains cas, l'hébergeur s'en charge automatiquement ou il faut l'activer via l'administration de l'hébergeur (c'est par exemple le cas avec Gandi).

Si vous devez procéder à cette redirection manuellement, utilisez le code suivant en remplaçant monsite.com par votre site :

```
# Redirection du site sans www vers www
RewriteEngine On
RewriteCond %{HTTP_HOST} ^monsite.com [NC]
RewriteRule ^(.*)$ http://www.monsite.com/$1 [L,R=301]
```

Remplacez monsite.com par votre nom de domaine

### Rediriger l'adresse avec www vers celle sans www

À l'inverse, si vous ne voulez pas des www devant le nom de votre site (comme pour WPMarmite), il est possible de faire une redirection vers la version sans les www.

Insérez le code suivant dans le fichier `.htaccess` :

```
# Redirection du site avec www vers la version sans www
RewriteEngine on
RewriteCond %{HTTP_HOST} ^www\.monsite\.com [NC]
RewriteRule ^(.*)$ http://monsite.com/$1 [L,R=301]
```

Remplacez monsite.com par votre nom de domaine

**Attention** : N'utilisez pas ce code avec le précédent sinon votre site souffrira d'une boucle de redirection (car la version sans www redirigera vers la version avec `www` qui redirigera vers la version sans `www`, etc.)

### Rediriger vers HTTPS

Si vous avez mis en place un certificat SSL sur votre site [pour le passer en HTTPS](https://wpmarmite.com/wordpress-https/), vous devez être certain que tous vos visiteurs naviguent bien sur la version sécurisée de votre site.

Dans le cas contraire, des informations sensibles pourraient être récupérées par des pirates (des données personnelles ou bancaires, par exemple).

Utilisez le code suivant pour passer tout votre site en HTTPS :

```
# Redirection vers HTTPS
RewriteCond     %{SERVER_PORT} ^80$
RewriteRule     ^(.*)$ https://%{SERVER_NAME}%{REQUEST_URI} [L,R]
```

### Forcer le téléchargement de fichiers spécifiques

Lorsque l'on désire télécharger un fichier à partir d'un site, notre navigateur essaie parfois de l'ouvrir pour l'afficher.

Personnellement, je trouve cela pratique pour les fichiers PDF en revanche, c'est très désagréable pour d'autres types de fichiers.

Insérez le code suivant pour que vos visiteurs téléchargent directement les fichiers dotés de ces extensions (modifiez-les à votre guise) :

```
# Forcer le téléchargement pour ces types de fichiers
AddType application/octet-stream .doc .docx .xls .xlsx .csv .mp3 .mp4
```

### Créer une page de maintenance personnalisée

Dans un précédent article, vous avez découvert [une sélection d'extensions de maintenance](https://wpmarmite.com/coming-soon-wordpress/). Pourtant, il y a des cas où la page de maintenance ne pourra pas s'afficher.

C'est fâcheux, n'est-ce pas ?

Pour bénéficier d'une page de maintenance, vous pouvez utiliser le code suivant :

```
# Page de maintenance
RewriteEngine on
RewriteCond %{REQUEST_URI} !/maintenance.html$
RewriteCond %{REMOTE_ADDR} !^xxx\.xxx\.xxx\.xxx
RewriteRule $ /maintenance.html [R=302,L]
```

Pour que cela fonctionne, vous devez :

-   Créer un fichier maintenance.html avec du contenu indiquant que le site est en maintenance
-   Ajouter votre adresse IP dans la ligne 4 (en gardant bien les « \ ») pour vous permettre d'accéder au site ([découvrez votre adresse IP sur ce site](http://www.mon-ip.com/info-adresse-ip.php))

Quand la maintenance sera terminée, mettez des « # » devant chaque ligne pour les passer en commentaire.

### Activer la mise en cache

Le fichier `.htaccess` permet de mettre en cache certains fichiers de votre site dans le navigateur de vos visiteurs pour que le chargement soit plus rapide.

En effet, le navigateur n'aura pas besoin de retélécharger les fichiers présents dans son cache.

Pour ce faire, insérez le code suivant :

```
# Mise en cache des fichiers dans le navigateur
<IfModule mod_expires.c>
ExpiresActive On
ExpiresDefault "access plus 1 month"

ExpiresByType text/html "access plus 0 seconds"
ExpiresByType text/xml "access plus 0 seconds"
ExpiresByType application/xml "access plus 0 seconds"
ExpiresByType application/json "access plus 0 seconds"
ExpiresByType application/pdf "access plus 0 seconds"

ExpiresByType application/rss+xml "access plus 1 hour"
ExpiresByType application/atom+xml "access plus 1 hour"

ExpiresByType application/x-font-ttf "access plus 1 month"
ExpiresByType font/opentype "access plus 1 month"
ExpiresByType application/x-font-woff "access plus 1 month"
ExpiresByType application/x-font-woff2 "access plus 1 month"
ExpiresByType image/svg+xml "access plus 1 month"
ExpiresByType application/vnd.ms-fontobject "access plus 1 month"

ExpiresByType image/jpg "access plus 1 month"
ExpiresByType image/jpeg "access plus 1 month"
ExpiresByType image/gif "access plus 1 month"
ExpiresByType image/png "access plus 1 month"

ExpiresByType video/ogg "access plus 1 month"
ExpiresByType audio/ogg "access plus 1 month"
ExpiresByType video/mp4 "access plus 1 month"
ExpiresByType video/webm "access plus 1 month"

ExpiresByType text/css "access plus 6 month"
ExpiresByType application/javascript "access plus 6 month"

ExpiresByType application/x-shockwave-flash "access plus 1 week"
ExpiresByType image/x-icon "access plus 1 week"

</IfModule>

# En-têtes
Header unset ETag
FileETag None

<ifModule mod_headers.c>
<filesMatch "\.(ico|jpe?g|png|gif|swf)$">
    Header set Cache-Control "public"
</filesMatch>
<filesMatch "\.(css)$">
    Header set Cache-Control "public"
</filesMatch>
<filesMatch "\.(js)$">
    Header set Cache-Control "private"
</filesMatch>
<filesMatch "\.(x?html?|php)$">
    Header set Cache-Control "private, must-revalidate"
</filesMatch>
</ifModule>
```

La mise en cache des fichiers sera effective pendant la durée spécifiée pour chaque type de fichier où jusqu'à ce que le visiteur vide son cache.

Pour accélérer votre site grâce à la mise en cache, je vous conseille [l'utilisation du plugin premium WP Rocket](https://wpmarmite.com/wprocket/). Simple et rapide à configurer, il convient parfaitement aux débutants sur WordPress.

### Activer la compression

En plus de tout ce que nous avons vu jusqu'à présent, il est possible de compresser certaines ressources avant qu'elles ne soient transférées du serveur au navigateur.

Et qui dit compression de fichier, dit vitesse d'affichage plus rapide pour la page. Je vous recommande donc de mettre en place ce code pour donner un coup d'accélérateur à votre site :

```
# Compressions des fichiers statiques
<IfModule mod_deflate.c>
    AddOutputFilterByType DEFLATE text/xhtml text/html text/plain text/xml text/javascript application/x-javascript text/css
    BrowserMatch ^Mozilla/4 gzip-only-text/html
    BrowserMatch ^Mozilla/4\.0[678] no-gzip
    BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
    SetEnvIfNoCase Request_URI \.(?:gif|jpe?g|png)$ no-gzip dont-vary
    Header append Vary User-Agent env=!dont-vary
</IfModule>

AddOutputFilterByType DEFLATE text/html
AddOutputFilterByType DEFLATE text/plain
AddOutputFilterByType DEFLATE text/xml
AddOutputFilterByType DEFLATE text/css
AddOutputFilterByType DEFLATE text/javascript
AddOutputFilterByType DEFLATE font/opentype
AddOutputFilterByType DEFLATE application/rss+xml
AddOutputFilterByType DEFLATE application/javascript
AddOutputFilterByType DEFLATE application/json
```

### Désactiver l'accès à certains scripts

Pour fonctionner, WordPress utilise des scripts situés dans le répertoire `wp-includes`, cependant il n'y a aucune raison d'y accéder directement. Utilisez ce code pour en limiter l'accès :

```
# Bloquer l'utilisation de certains scripts
RewriteEngine On
RewriteBase /
RewriteRule ^wp-admin/includes/ - [F,L]
RewriteRule !^wp-includes/ - [S=3]
RewriteRule ^wp-includes/[^/]+\.php$ - [F,L]
RewriteRule ^wp-includes/js/tinymce/langs/.+\.php - [F,L]
RewriteRule ^wp-includes/theme-compat/ - [F,L]
```

Vous pourrez en savoir plus dans le [codex](https://codex.wordpress.org/Hardening_WordPress#Securing_wp-includes).

### Protection contre les injections de fichiers

Des pirates peuvent tenter d'envoyer des fichiers sur votre serveur pour prendre le contrôle de votre site. Pour leur mettre des bâtons dans les roues, vous pouvez inclure ce code dans votre fichier `.htaccess` :

```
# Protection contre les injections de fichiers
RewriteCond %{REQUEST_METHOD} GET
RewriteCond %{QUERY_STRING} [a-zA-Z0-9_]=http:// [OR]
RewriteCond %{QUERY_STRING} [a-zA-Z0-9_]=(\.\.//?)+ [OR]
RewriteCond %{QUERY_STRING} [a-zA-Z0-9_]=/([a-z0-9_.]//?)+ [NC]
RewriteRule .* - [F]
```

### Protection contre d'autres menaces

Sur Facebook, Richard m'a indiqué qu'il était possible de se prémunir du « clickjacking » et d'autres menaces en ajoutant quelques lignes dans le fichier `.htaccess`.

Pour info, le clickjacking est une technique qui permet de faire croire à un visiteur qu'il est sur votre site, alors que ce n'est pas le cas, grâce à des balises `frame` ou `iframe`.

Le code suivant permet donc de vous protéger contre le clickjacking, de lutter contre d'autres menaces comme [le MIME Sniffing](https://www.alsacreations.com/article/lire/1723-tour-horizon-https-et-en-tetes-de-securite.html), et de bloquer le contenu en cas d'attaque XSS ([qui va injecter du code HTML ou JavaScript dans des variables mal protégées](https://openclassrooms.com/fr/courses/2091901-protegez-vous-efficacement-contre-les-failles-web/2680167-la-faille-xss)).

```
# Protections diverses (XSS, clickjacking et MIME-Type sniffing)
<ifModule mod_headers.c>
Header set X-XSS-Protection "1; mode=block"
Header always append X-Frame-Options SAMEORIGIN
Header set X-Content-Type-Options: "nosniff"
</ifModule>
```

Et voilà, vous venez de faire le tour de tout un tas d'optimisations à intégrer dans le fichier .htaccess situé à la racine du site.