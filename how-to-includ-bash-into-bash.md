How to Include Bash Script in other Bash Script
===============================================

Written by [Rahul](https://tecadmin.net/author/myadmin/ "Posts by Rahul"), Updated on July 26, 2016

 [bash](https://tecadmin.net/tag/bash/), [script](https://tecadmin.net/tag/script/) 

Bash scripts are very useful for doing work easier. It also helps for task automation. This tutorial will help you to how to include bash script in other bash script.

Create Sample Scripts
---------------------

For example, I am creating two scripts, first is config.sh which contains some variables. Second script is our main script main.sh, which includes first script and used variables defines there.

Step 1 -- First Script (config.sh)

USERNAME="rahul"
EMAIL="rahul@tecadmin.net"

Main Script (main.sh)

#!/bin/bash

# Including config.sh, set filename with proper path.

source config.sh

echo Welcome ${USERNAME}!
echo Your email is ${EMAIL}.

Step 2 -- Execute Script
-----------------------

Let's execute main.sh using terminal and watch the results.

[root@tecadmin ~]$ sh main.sh

Welcome rahul!
Your email is rahul@tecadmin.net.