If you have a VPS to manage, chances are you also have installed a [Hosting Control Panel](https://en.wikipedia.org/wiki/Web_hosting_control_panel) which helps you visually do that. There are many solutions in the market, some paid, other free. I'm using the free [VestaCP](http://vestacp.com/) in both of my client's servers, as well as in my own VPS and I'd recommend you to have a look in it.

Also, I'm used to having [CloudFlare](https://www.cloudflare.com/) to manage the DNS settings, as well as given me a huge hand with content distribution and caching. You can even start using it for free!

However, there is a problem when you have this combination. By default, VestaCP listen to the port 8083 for its interface, while CloudFlare does not forward such port on the free plan! If you chose to keep using the standard port, it's absolutely fine because you can always access your panel with the server IP address.

But, if you'd like to get your team accessing the panel using one domain name (e.g: your-business.com) then, you'll need to keep reading a bit more! 😉

[Check here all the ports CloudFlare supports on the free plan](https://support.cloudflare.com/hc/en-us/articles/200169156-Which-ports-will-Cloudflare-work-with-). If you strictly need support to any other port, then consider upgrading to a paid plan! It starts with as little as $20/ mo (as for July of 2019).

But, if you want to use VestaCP and still would like to stick with the free plan, then you can always change the port used by the Hosting Control Panel. You can choose any one of the supported ports from CloudFlare, for instance, "2096".

1 Step: Adding a new firewall rule

We need this to enable the firewall accepting traffic in the chosen port. If you're not using a firewall on your machine (highly unrecommended), skip this step.

Go to https://[your-server-ip]:8083/list/firewall/ and click over the "Add rule" button, and fill the form like the example below and complete by hitting the "Add" button.

![](https://miro.medium.com/max/50/1*9VF1QBkdXaf7-9Nu0TqDug.png?q=20)

![](https://miro.medium.com/max/902/1*9VF1QBkdXaf7-9Nu0TqDug.png)

(VestaCP Firewall Add-Rule Form)

All done! Time for changing the Nginx settings and restart VestaCP service.

2 Step: Changing Nginx configuration:

Open a new terminal window and connect to your server over SSH. Note that this should be done using an account with sufficient privilege to editing files under `/usr/local`.

Just open the config file:

$ editor /usr/local/vesta/nginx/conf/nginx.conf

Search for the text `listen`. You can do it by pressing `CTRL + W` ...

Once you find the line, change the port number from `8083` to `2096` (or any other port number you choose). The final result will look something like:

# Vhost\
server {\
    listen 2096;\
    ...\
}

All good, just save the config file. You can do it by pressing `CTRL + X` and typing `Y` followed by `ENTER` to confirm the file's changes.

3 Step: Restarting VestaCP service

All you need to do now is restart the service to get it working properly. The service can be restarted with:

/etc/init.d/vesta restart

All done! Now you should be able to access the VestaCP thru your domain name and the new defined port, e.g: `<https://your-domain.com:2096/>`

Good job!