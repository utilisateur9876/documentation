Introduction à Firebase 🔥
==========================

by [Maxime Chenot](https://les-veilleurs-de-nuit.herokuapp.com/author/maxime/)

2018-04-26

![](https://res-3.cloudinary.com/hsxjjsoev/image/upload/q_auto/v1/ghost-blog-images/pic-firebase-intro.jpg)

> Cet article est une adaptation de [Introduction to Firebase 🔥](https://hackernoon.com/introduction-to-firebase-218a23186cd7) écrit à l'origine par [GeekyAnts](https://hackernoon.com/@geekyants)

Un petit post à propos de ce qu'est Firebase et sa nouvelle base de données NoSQL - Cloud Firestore.

Avec une variété de technologies côté serveur qui sont sur le marché aujourd'hui, les développeurs ont du mal à décider quel type de backend est le plus approprié pour leur application.

Dans ce post, nous allons explorer l'un de ces choix qui s'appelle Firebase 🔥, ainsi que tous les outils et services qu'il fournit.

Firebase
--------

![](https://cdn-images-1.medium.com/max/720/1*t3Eh571oT4I37kOCWD6tuQ.png)

Firebase est une plate-forme de développement d'applications mobiles et Web qui fournit aux développeurs une pléthore d'outils et de services pour les aider à développer des applications de haute qualité, à élargir leur base d'utilisateurs et à générer davantage de profits.

### Les origines

En 2011, avant que Firebase ne devienne Firebase, c'était une startup appelée Envolve. En tant qu'Envolve, il fournissait aux développeurs une API qui permettait d'intégrer une fonctionnalité de chat en ligne sur leur site Web.

Ce qui est intéressant, c'est que les gens utilisaient Envolve pour transmettre des données d'application qui n'étaient pas que des messages de discussion. Les développeurs utilisaient Envolve pour synchroniser les données d'application, telles que l'état d'un jeu en temps réel avec leurs utilisateurs.

Cela a conduit les fondateurs d'Envolve, [James Tamplin](https://twitter.com/JamesTamplin) et [Andrew Lee](https://twitter.com/startupandrew), à séparer le système de discussion et l'architecture en temps réel. En avril 2012, Firebase a été créée en tant qu'entreprise indépendante fournissant des fonctionnalités en temps réel en tant que Backend-as-a-Service.

Après son acquisition par Google en 2014, Firebase a rapidement évolué pour devenir le monstre multifonctionnel pour plate-forme mobile et web qu'elle est aujourd'hui.

### Les services de Firebase

Les services de Firebase peuvent être divisés en deux groupes :

![](https://cdn-images-1.medium.com/max/720/1*rIDMNpRx8HnnY5mhhbXLuA.png)

### Développer et tester son application :

-   [Realtime Database](https://firebase.google.com/docs/database/)
-   [Auth](https://firebase.google.com/docs/auth/)
-   [Test Lab](https://firebase.google.com/docs/test-lab/)
-   [Crashlytics](https://firebase.google.com/docs/crashlytics/)
-   [Cloud Functions](https://firebase.google.com/docs/functions/)
-   [Firestore](https://firebase.google.com/docs/firestore/)
-   [Cloud Storage](https://firebase.google.com/docs/storage/)
-   [Performance Monitoring](https://firebase.google.com/docs/perf-mon/)
-   [Crash Reporting](https://firebase.google.com/docs/crash/)
-   [Hosting](https://firebase.google.com/docs/hosting/)

### Grandir et engager son public :

-   [Firebase Analytics](https://firebase.google.com/docs/analytics/)
-   [Invites](https://firebase.google.com/docs/invites/)
-   [Cloud Messaging](https://firebase.google.com/docs/cloud-messaging/)
-   [Predictions](https://firebase.google.com/docs/predictions/)
-   [AdMob](https://firebase.google.com/docs/admob/)
-   [Dynamic Links](https://firebase.google.com/docs/dynamic-links/)
-   [Adwords](https://firebase.google.com/docs/adwords/)
-   [Remote Config](https://firebase.google.com/docs/remote-config/)
-   [App Indexing](https://firebase.google.com/docs/app-indexing/)

Firebase Realtime Database
--------------------------

Firebase Realtime Database est une base de données NoSQL hébergée dans le cloud qui vous permet de stocker et de synchroniser des données entre vos utilisateurs en temps réel.

Realtime Database est vraiment juste un gros objet JSON que les développeurs peuvent gérer en temps réel.

![](https://cdn-images-1.medium.com/max/720/1*kiliGRYQIVzsCTx_JsUYdg.png)\
Realtime Database => Un arbre de valeurs

Simplement avec une API, Firebase Realtime Database fournit à votre application à la fois la valeur actuelle des données et les éventuelles mises à jour de ces données.

La synchronisation en temps réel permet à vos utilisateurs d'accéder facilement à leurs données depuis n'importe quel appareil, que ce soit sur le Web ou sur un appareil mobile. La base de données en temps réel permet également à vos utilisateurs de collaborer les uns avec les autres.

Un autre gros avantage de Realtime Database est qu'elle est livrée avec des SDK mobiles et Web, vous permettant de créer vos applications sans avoir besoin de serveurs.

Lorsque vos utilisateurs sont hors ligne, les SDK de base de données en temps réel utilisent le cache local sur l'appareil pour servir et stocker les modifications. Lorsque l'appareil est en ligne, les données locales sont automatiquement synchronisées.

Realtime Database peut également s'intégrer à l'authentification Firebase pour fournir un processus d'authentification simple et intuitif.

Firebase Authentication
-----------------------

![](https://cdn-images-1.medium.com/max/720/1*diDbAK_1OnFXz90jsdzQBw.png)

Firebase Authentication fournit des services backend, des SDK faciles à utiliser et des bibliothèques d'interfaces utilisateur prêtes à l'emploi pour authentifier les utilisateurs de votre application.

Normalement, il vous faudrait des mois pour configurer votre propre système d'authentification. Et même après cela, vous devrez garder une équipe dédiée pour maintenir ce système. Mais si vous utilisez Firebase, vous pouvez configurer l'ensemble du système en moins de 10 lignes de code qui gérera tout pour vous, y compris les opérations complexes comme la fusion de comptes.

Vous pouvez authentifier les utilisateurs de votre application à l'aide des méthodes suivantes:

-   Email & mot de passe
-   Numéro de téléphone
-   Google
-   Facebook
-   Twitter
-   etc!

L'utilisation de Firebase Authentication facilite la création de systèmes d'authentification sécurisés, tout en améliorant l'expérience de connexion et d'intégration pour les utilisateurs finaux.

Firebase Authentication est créée par les mêmes personnes qui ont créé Google Sign-in, Smart Lock et Chrome Password Manager.

Firebase Cloud Messaging (FCM)
------------------------------

![](https://cdn-images-1.medium.com/max/720/1*HtZwrGjaV2kJ4tu9D_phuw.png)

Firebase Cloud Messaging (FCM) fournit une connexion fiable et à faible consommation de batterie entre votre serveur et vos périphériques, vous permettant d'envoyer et de recevoir gratuitement des messages et des notifications sur iOS, Android et sur le Web.

Vous pouvez envoyer des messages de notification (limite de 2 Ko) et des messages de données (limite de 4 Ko).

En utilisant FCM, vous pouvez facilement cibler les messages en utilisant des segments prédéfinis ou créer les vôtres, en utilisant les données démographiques et comportementales. Vous pouvez envoyer des messages à un groupe d'appareils abonnés à des rubriques spécifiques, ou vous pouvez obtenir des informations aussi détaillées qu'un seul appareil.

FCM peut envoyer des messages instantanément, ou à un moment ultérieur dans le fuseau horaire local de l'utilisateur. Vous pouvez envoyer des données d'application personnalisées, telles que la définition des priorités, des sons et des dates d'expiration, ainsi que le suivi des événements de conversion.

![](https://cdn-images-1.medium.com/max/720/1*aJOTSR9_5coSiJUmiqwlJQ.png)

Vous pouvez également utiliser le test A / B pour essayer différentes versions de vos messages de notification, puis sélectionner celui qui correspond le mieux à vos objectifs.

Firebase Database Query
-----------------------

Firebase a simplifié le processus de récupération des données spécifiques de la base de données via des requêtes. Les requêtes sont créées en chaînant une ou plusieurs méthodes de filtrage.

Firebase dispose de 4 fonctions de tri:

-   orderByKey()
-   orderByChild('child')
-   orderByValue()
-   orderByPriority()

Notez que vous ne recevrez les données d'une requête que si vous avez utilisé la méthode `on ()` ou `once ()`.

Vous pouvez également utiliser ces fonctions d'interrogation avancées pour restreindre davantage les données:

-   startAt('value')
-   endAt('value')
-   equalTo('child_key')
-   limitToFirst(10)
-   limitToLast(10)

En SQL, les bases de l'interrogation impliquent deux étapes. D'abord, vous sélectionnez les colonnes de votre table. Ici, je sélectionne la colonne `Users`. Ensuite, vous pouvez appliquer une restriction à votre requête en utilisant la clause `WHERE`. De la requête ci-dessous, j'obtiendrai une liste d'utilisateurs dont le nom est `GeekyAnts`.

![](https://cdn-images-1.medium.com/max/800/1*ejsjUoIDe4xBf0q2XKuwtg.png)

Vous pouvez également utiliser la clause `LIMIT`, qui limitera le nombre de résultats que vous obtiendrez à partir de votre requête.

![](https://cdn-images-1.medium.com/max/800/1*IygnNXSDrSmRySgY_WaIoQ.png)

Dans Firebase, l'interrogation implique également deux étapes. D'abord, vous créez une référence à la clé parente, puis vous utilisez une fonction de tri. En option, vous pouvez également ajouter une fonction d'interrogation pour une restriction plus avancée.\
![](https://cdn-images-1.medium.com/max/800/1*pZ4aFsOFf4UnkxhGjmr_Bw.png)

Comment stocker des données ? => Firebase Storage
-------------------------------------------------

Firebase Storage est une solution autonome permettant de télécharger du contenu généré par les utilisateurs, comme des images et des vidéos, à partir d'un appareil iOS et Android, ainsi que du Web.

Le stockage Firebase est conçu spécifiquement pour **mettre à l'échelle vos applications, assurer la sécurité et assurer la résilience du réseau**

Firebase Storage utilise un simple système de dossiers / fichiers pour structurer ses données.

![](https://cdn-images-1.medium.com/max/800/1*JrdmCP5xgq4-l6thCtk3lg.png)

Firebase Test Labs
------------------

Firebase Test Labs fournit un grand nombre de périphériques de test mobiles pour vous aider à tester vos applications.

Firebase Test Labs est livré avec 3 modes de test:

### Instrumentation Test

Ce sont des tests que vous avez écrits spécifiquement pour tester votre application, en utilisant des frameworks comme Espresso et UI Automator 2.0

### Robo Test

Ce test est pour les personnes qui veulent juste se détendre et laisser Firebase s'inquiéter des tests. Firebase Test Labs peut simuler le toucher de l'utilisateur et voir comment fonctionne chaque composant de l'application.

### Game Loop Test

Test Labs prend en charge le test des applications de jeu. Il est livré avec un support bêta pour l'utilisation d'un "mode démo" où l'application de jeu s'exécute en simulant les actions du joueur.

Remote Config
-------------

![](https://cdn-images-1.medium.com/max/800/1*m8pkhJHEkBdxYtCMNOqwLA.png)

Remote config nous permet essentiellement de publier des mises à jour à nos utilisateurs immédiatement. Si on souhaite changer la palette de couleurs pour un écran, la disposition pour une section particulière dans notre application ou afficher des options promotionnelles / saisonnières - c'est complètement faisable en utilisant les paramètres côté serveur sans avoir besoin de publier une nouvelle version.

Remote Config nous donne le pouvoir de:

-   Rapidement et facilement mettre à jour nos applications sans avoir besoin de publier une nouvelle version de l'app / play store.
-   Définir sans effort le comportement ou l'apparence d'un segment dans notre application en fonction de l'utilisateur / périphérique qui l'utilise.

Firebase App Indexing
---------------------

![](https://cdn-images-1.medium.com/max/800/1*hyd0QNoDUL3Hn5_EWxlbuQ.png)

Pour que le contenu de votre application soit indexé par Google, utilisez les mêmes URL dans votre application que vous utilisez sur votre site Web et assurez vous d'être le propriétaire de votre application et de votre site Web. Google Search explore les liens sur votre site Web et les sert dans les résultats de recherche. Ensuite, les utilisateurs qui ont installé votre application sur leurs appareils accèdent directement au contenu de votre application lorsqu'ils cliquent sur un lien.

Firebase Dynamic Links
----------------------

Les liens profonds sont des URL qui vous mènent à un contenu. La plupart des liens web sont des liens profonds.

Firebase peut maintenant modifier les liens profonds en liens dynamiques! Les liens dynamiques permettent à l'utilisateur d'accéder directement à un emplacement particulier de votre application.

Il y a 3 utilisations fondamentales pour les liens dynamiques :

-   Convertir les utilisateurs Web mobiles en utilisateurs d'applications natives.\
    ![](https://cdn-images-1.medium.com/max/800/1*LQ833FNLxVKDQWCntT4unA.png)

-   Augmentez la conversion pour le partage utilisateur-utilisateur. En convertissant les utilisateurs de votre application, lorsque l'application est partagée avec d'autres utilisateurs, vous pouvez ignorer le message générique affiché lorsqu'un utilisateur le télécharge depuis le store. Au lieu de cela, vous pouvez leur montrer un message d'accueil personnalisé.\
    ![](https://cdn-images-1.medium.com/max/800/1*B7WNKo4xblak-Km53ahYEA.png)

-   Le lecteur s'installe à partir de service tiers. Vous pouvez utiliser les réseaux sociaux, les e-mails et les SMS pour augmenter votre public cible. Lorsque les utilisateurs installent l'application, ils peuvent voir le contenu exact de vos campagnes.\
    ![](https://cdn-images-1.medium.com/max/800/1*lNZD2wtZ45jpnwqqzJFNFA.png)

Firestore
---------

![](https://cdn-images-1.medium.com/max/800/1*XYVH_gJPV_Vsn9mx8jmriA.png)

Cloud Firestore est une base de données de documents NoSQL qui vous permet de facilement stocker, synchroniser et interroger des données pour vos applications mobiles et Web - à l'échelle mondiale.

Bien que cela puisse ressembler à quelque chose de similaire à la base de données en temps réel, Firestore apporte beaucoup de nouvelles choses à la plate-forme qui en fait quelque chose de complètement différent de Realtime Database.

### Requête améliorée et structure de données

Là où Realtime Database stocke des données sous la forme d'un arbre JSON géant, Cloud Firestore adopte une approche beaucoup plus structurée. Firestore conserve ses données dans des objets appelés documents. Ces documents sont constitués de paires clé-valeur et peuvent contenir n'importe quel type de données, depuis les chaînes jusqu'aux données binaires en passant par des objets qui ressemblent à des arbres JSON (Firestore l'appelle des maps). Les documents, à leur tour, sont regroupés en collections.

![](https://cdn-images-1.medium.com/max/800/1*L7MSpewbvTyZeYukfYXNfw.png)

La base de données Firestore peut se composer de plusieurs collections qui peuvent contenir des documents pointant vers des sous-collections. Ces sous-collections peuvent à nouveau contenir des documents qui pointent vers d'autres sous-collections, et ainsi de suite.

![](https://cdn-images-1.medium.com/max/800/1*axdF1x2DJKVDLjAmL7NIXA.png)

Vous pouvez créer des hiérarchies pour stocker les données associées et récupérer facilement les données dont vous avez besoin à l'aide de requêtes. Toutes les requêtes peuvent évoluer en fonction de la taille de votre jeu de résultats. Votre application est donc prête à évoluer depuis le premier jour.

Les requêtes Firestore sont *peu profondes*. Par ceci, je veux dire que dans Firestore, vous pouvez simplement chercher n'importe quel document que vous voulez sans avoir à récupérer toutes les données contenues dans l'une de ses sous-collections liées.

![](https://cdn-images-1.medium.com/max/800/1*Jokb0HxrnEXGbBF9Slwf1w.png)\
Vous pouvez récupérer un seul document sans avoir à récupérer l'une de ses sous-collections

### Requêtes avec Firestore

Imaginez que vous avez créé une collection dans Firestore contenant une liste de villes. Ainsi, avant de pouvoir envoyer une requête, vous devrez stocker la base de données dans une variable.

![](https://cdn-images-1.medium.com/max/800/1*7NpYwj-iHHrVnKXP6654OQ.png)

Ici, `citiesRef` est cette variable qui contient votre collection de villes. Maintenant, si vous voulez trouver une liste de capitales, vous devez écrire une requête comme celle-ci:

![](https://cdn-images-1.medium.com/max/800/1*-zq4-6sPr88GDYIPB7_sIg.png)

Voici un autre exemple de requêtes dans Firestore. Dites que vous voulez voir seulement 2 des villes de votre base de données dont la population est plus de 100 000.

![](https://cdn-images-1.medium.com/max/800/1*cX5C0ECHjTe9BCIe1ihZcw.png)

Mais Cloud Firestore peut rendre l'interrogation encore plus facile! Dans certains cas, Cloud Firestore peut rechercher automatiquement dans plusieurs champs de votre base de données. Firestore vous guidera vers la construction automatique d'un index qui aidera Firestore à rendre l'interrogation extrêmement simple.

![](https://cdn-images-1.medium.com/max/800/1*bP5vVdaap87ciuzdLxcJ8g.png)

### Meilleure évolution

Bien que Firebase Realtime Database puisse évoluer, les choses vont devenir folles quand votre application deviendra vraiment populaire ou si votre base de données devient vraiment massive.

Cloud Firestore est basé sur l'infrastructure Google Cloud. Cela lui permet d'évoluer beaucoup plus facilement et à une plus grande capacité que Realtime Database.

### Base de données multi-régions

Dans Firestore, vos données sont automatiquement copiées dans différentes régions. Ainsi, si un centre de données se déconnecte pour une raison imprévue, vous pouvez être sûr que les données de votre application sont toujours en sécurité ailleurs.

La base de données multi-régions de Firestore offre également une forte cohérence. Toute modification de vos données sera reflétée dans chaque copie de votre base de données.

### Différents modèles de prix

Realtime Database charge ses utilisateurs en fonction de la quantité de données que vous avez stockée dans la base de données.

Cloud Firestore vous facture également la même chose, mais le coût est nettement inférieur à celui de Realtime Database et au lieu de baser le coût sur la quantité de données stockées, le prix de Firestore dépend du nombre de lectures / écritures que vous effectuez.

Consultez cet article pour en savoir plus sur Cloud Firestore :

[**Cloud Firestore for Realtime Database Developers**\
*Hey, did you hear the big news? We just announced the beta release of Cloud Firestore -- the new database that lets you...*firebase.googleblog.com](https://firebase.googleblog.com/2017/10/cloud-firestore-for-rtdb-developers.html "https://firebase.googleblog.com/2017/10/cloud-firestore-for-rtdb-developers.html")

Vous pouvez également parcourir ce codelab pour mieux comprendre le fonctionnement de Cloud Firestore :

[**Cloud Firestore Web Codelab**\
*We're almost there--before we can write documents to Firestore we need to open up Firestore's security rules and...*codelabs.developers.google.com](https://codelabs.developers.google.com/codelabs/firestore-web/index.html?index=..%2F..%2Fgdd17#0 "https://codelabs.developers.google.com/codelabs/firestore-web/index.html?index=..%2F..%2Fgdd17#0")

Dernières nouveautés de Firebase
--------------------------------

### Robo Scripts

Firebase Test Labs a ce service étonnant appelé **Robo Test** qui nous permet de tester notre application sans avoir à écrire de script de test. Avec Robo Test, Firebase permet de tester complètement votre application, voire de remplir des champs de formulaire spécifiques et des boutons poussoirs!

Maintenant Firebase a mis au point une autre fonctionnalité de test appelée **Robo Scripts**. Avec Robo Scripts, vous pouvez enregistrer une série d'actions pour que Firebase prenne votre application.

Lorsque vous exécutez un test Robo avec un script Robo attaché, Firebase commence par parcourir les actions enregistrées depuis le script Robo, puis explore l'application comme d'habitude.

### Firebase Predictions

Firebase intègre désormais l'apprentissage automatique, qui permet d'analyser les données de votre application et de créer des groupes d'utilisateurs dynamiques en fonction du comportement prévu de l'utilisateur.

Firebase Predictions peut fonctionner avec Remote Config pour augmenter les conversions en fournissant une expérience personnalisée basée sur le comportement de chaque utilisateur.

Ou, il peut travailler avec Notifications composer pour livrer le bon message au bon groupe d'utilisateurs.

Firebase Predictions peut également fonctionner de pair avec les tests A / B pour évaluer l'efficacité de vos stratégies basées sur les prévisions.