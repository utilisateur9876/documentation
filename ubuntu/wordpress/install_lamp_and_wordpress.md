https://www.hostinger.com/tutorials/how-to-install-wordpress-on-ubuntu-using-lamp-stack/

How to Install WordPress on Ubuntu 18.04 Using LAMP Stack
=========================================================

WordPress is undoubtedly one of the most popular Content Management Systems (CMS) available in the market. WordPress can be installed in multiple ways -- one of them is using LAMP. With this method, you can host your very own WordPress website on a [VPS](https://www.hostinger.com/tutorials/what-is-vps-hosting). That's why in this tutorial, we'll show you how to install WordPress on Ubuntu 18.04.

This method is mostly used when you want to have full control of the WordPress backend for experimentation and advanced uses. More often than not, users keep LAMP stack for their WordPress development environment.

LAMP stands for Linux, Apache, MySQL, and PHP. Linux is the operating system with [Apache](https://www.hostinger.com/tutorials/what-is-apache) web server and [MySQL](https://www.hostinger.com/tutorials/what-is-mysql) Database that uses [PHP](https://www.hostinger.com/tutorials/best-php-framework) to process dynamic website content.

Through this tutorial, we will show you how to install WordPress on Ubuntu 18.04 using LAMP stack.

Protip: Always make sure you have the required sudo access since this method calls for a couple of installations and a basic firewall configuration.

Remember, before you install WordPress on Ubuntu, you need to access your VPS using SSH. Check out our [PuTTY tutorial](https://www.hostinger.com/tutorials/how-to-connect-to-your-account-using-putty) if you're having trouble.

Install and Configure Apache 2
------------------------------

LAMP uses Apache web server 2, which is popular and widely used. You can install Apache using Ubuntu's package manager, as shown below:

sudo apt update
sudo apt install apache2

These commands require you to provide passwords. Press **Y** on the command prompt and hit **ENTER** to continue the installation.

UFW firewall is available in all the Ubuntu installations by default. The UFW firewall should allow HTTP and HTTPS traffic. If you don't use UFW, feel free to skip this step. You can check the UFW application profiles using the following command:

sudo ufw app list

This will provide an output as:

Available applications:
 Apache
 Apache Full
 Apache Secure
 OpenSSH

To allow HTTP and HTTPS traffic for Apache Full profile, you can execute the command below:

sudo ufw allow in "Apache Full"

To confirm this, have a look at this profile information by using:

sudo ufw app info "Apache Full"

To verify if Apache was installed correctly, you would need the public IP address. You can get it by using the command below:

ip addr show eth0 | grep inet | awk '{ print $2; }' | sed 's/\/.*$//'

This command will provide you with 2-3 IPs. You might have to try all of this by using http://your_public_IPAddress on a browser. It will display the default Ubuntu Apache page.

Install MySQL
-------------

Once you have the webserver running, you can install the MySQL database. The command below will help you with the task:

sudo apt install mysql-server

This command will prompt for a password. Once the installation starts, based on your requirement, you can press **Y** and **ENTER,** which completes the installation. Open MySQL terminal using:

sudo mysql

You can set a password for the root user using the command below:

mysql> ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Password;

To reflect these changes, use the Flush command, as shown below:

mysql> FLUSH PRIVILEGES;

You can type exit to come back from the MySQL prompt. This completes the MySQL installation.

Install PHP
-----------

To display dynamic content, you need PHP which you can install by using:

sudo apt install php libapache2-mod-php php-mysql

Install Additional PHP extensions for WordPress using the command below:

sudo apt install php-curl php-gd php-xml php-mbstring  php-xmlrpc php-zip php-soap php-intl

When you request a directory, the index.html is displayed as a default setting. In case you want to show **index.php** instead of **index.html**, you need to open the dir.conf file using the **vi** editor:

sudo vi /etc/apache2/mods-enabled/dir.conf

It will look as shown below:

<IfModule mod_dir.c>
   DirectoryIndex index.html index.cgi index.pl index.php index.xhtml index.htm
</IfModule>

Swap the positions of **index.html** and **index.php** and save the file.

For the changes to be visible, restart the Apache server using:

sudo systemctl restart apache2

To test the PHP, you can create a sample PHP file s**ample.php** and add these lines of code:

<?php
phpinfo();
?>

This file needs to be added to the Web Root of Apache which is located at -- **/var/www/html/**

Once this is saved, you can try to access this page by using **http://Your_Public_IP/Sample.php**.

MySQL Setup for WordPress
-------------------------

You will have to login to MySQL as a root user. To do so, use the command below:

mysql -u root -p

You will get a prompt to type the password for root, which you had set up earlier. Once you have logged in, you can create a separate database for WordPress.

Below we have created a new DB called WordPressDB:

mysql> CREATE DATABASE WordPressDB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

Next, you will create a separate user for WordPress. Name this as WordPressUser. To create a new user and grant privileges using the command below:

mysql> GRANT ALL ON WordPressDB.* TO ' WordPressUser '@'localhost' IDENTIFIED BY 'NewPasswordToBeSet';

Provide a strong password in place of **NewPasswordToBeSet**. To reflect these changes, use the command:

mysql> FLUSH PRIVILEGES;

Once this is completed, exit the MySQL prompt.

Prepare to Install WordPress on Ubuntu
--------------------------------------

Create a configuration file, for example :WordPress.conf. Place it to **/etc/apache2/sites-available/**. This will be a replica of the default configuration file which already exists in this location.

Protip: Remember, all locale names in Linux are case sensitive!

Also, create a WordPress directory (or you can provide any other name at location **/var/www/**). The complete location would be **/var/www/wordpress**.

The file **WordPress.conf** will be the Apache configuration file for this testing. In the file, you can enable **.htaccess** by adding these lines to the VirtualHost block:

<Directory /var/www/wordpress />
   AllowOverride All
</Directory>

Once this is edited, save the file.

Next, you can enable the **mod_rewrite** to use the WordPress permalink feature. This can be done using:

sudo a2enmod rewrite

In the file **/etc/apache2/apache2.conf**, you can change the ServerName directive by providing the server's IP or hostname.

You can test this complete configuration by using the below command.

Protip: Ensure it gives the Syntax OK message. Any other additional message can be ignored, as this will not impact the installation.

sudo apache2ctl configtest

To see the changes made, restart the Apache server using the previously mentioned restart command:

sudo systemctl restart apache2

Configure and Install WordPress on Ubuntu
-----------------------------------------

You can change your location to a directory and start downloading WordPress. This can be done as shown below:

cd /tmp

curl -O https://wordpress.org/latest.tar.gz

Extract this file using:

tar xzvf latest.tar.gz

Here you can create a .htaccess file:

vi /tmp/wordpress/.htaccess

Save the file. Rename the **wp-config-****sample.php** file using the command:

mv /tmp/wordpress/wp-config-sample.php /tmp/wordpress/wp-config.php

Create the below folder at the given location:

mkdir /tmp/wordpress/wp-content/upgrade

This completes our initial setup, which can be copied to the document root.

sudo cp -a /tmp/wordpress/. /var/www/wordpress

To ensure everything works correctly, you will have to change the ownership of WordPress files to the www-data users and groups. Those are the users that Apache web server will use.

To change the ownership, you can use this command:

sudo chown -R www-data:www-data /var/www/wordpress

Also, you will have to set the correct permission to the directories and files:

sudo find /var/www/wordpress/ -type d -exec chmod 750 {} \;

sudo find /var/www/wordpress/ -type f -exec chmod 640 {} \;

For the initial configuration, it requires the WordPress salt to be generated. This can be done using:

curl -s https://api.wordpress.org/secret-key/1.1/salt/

It will give a different output every time and will contain a list of salt values. The output of the above command needs to be copied and added to the wp-config.php file.

vi /var/www/wordpress/wp-config.php

Replace the existing dummy values within this file. Save the file after making the changes.

The wp-config.php file contains some Database configuration at the top. Replace the **DB_NAME, DB_USER, DB_PASSWORD** with values you have created for WordPress.

define('DB_NAME', 'WordPressDB');

/** MySQL database username */
define('DB_USER', 'WordPressUser');
/** MySQL database password */
define('DB_PASSWORD', 'DB_Password');

Also, you can add the file system method at the very bottom, as shown below:

define('FS_METHOD', 'direct');

Save the file.

This completes the backend setup. Now you can access WordPress via the interface by using the URL **http://Your_IP_Address**.

What follows will be similar to the regular WordPress setup. First, you will be asked to select a language.

Next, you will be redirected to a page where you can set the username and password.

Be sure to change the username and password to improve WordPress security. By pressing the Install WordPress button, you will complete your WordPress setup and will be prompted to a login screen.

Provide the WordPress user name and password to access the dashboard.

Wrap Up
-------

With this, you now know how to install WordPress on Ubuntu 18.04 with the LAMP stack. From here, you can start exploring all the amazing WordPress features and develop some jaw-dropping websites.