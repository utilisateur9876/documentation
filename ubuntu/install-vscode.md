\
![](https://linuxize.com/post/how-to-install-visual-studio-code-on-ubuntu-18-04/featured_huc5600321387e9c86757776e87a4f2d66_24740_600x0_resize_q95_box.jpg)

[Visual Studio Code](https://code.visualstudio.com/) is an open-source cross-platform code editor developed by Microsoft. It has a built-in debugging support, embedded [Git](https://linuxize.com/post/how-to-install-git-on-ubuntu-18-04/) control, syntax highlighting, code completion, integrated terminal, code refactoring and snippets.

The easiest and recommended way to install Visual Studio Code on Ubuntu machines is to enable the VS Code repository and install the VS Code package through the command line.

Although this tutorial is written for Ubuntu 18.04 the same steps can be used for Ubuntu 16.04.

Prerequisites
-------------

Before continuing with this tutorial, make sure you are logged in as a [user with sudo privileges](https://linuxize.com/post/how-to-create-a-sudo-user-on-ubuntu/).

Installing Visual Studio Code on Ubuntu
---------------------------------------

To install Visual Studio Code on your Ubuntu system, follow these steps:

1.  First, update the packages index and install the dependencies by typing:

    ```
    sudo apt update
    ```

2.  Next, import the Microsoft GPG key using the following [wget command](https://linuxize.com/post/wget-command-examples/):

    ```
    wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
    ```

    And enable the Visual Studio Code repository by typing:

    ```
    sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
    ```

3.  Once the [apt repository is enabled](https://linuxize.com/post/how-to-add-apt-repository-in-ubuntu/), install the latest version of Visual Studio Code with:

    ```
    sudo apt install code
    sudo apt update
    ```

That's it. Visual Studio Code has been installed on your Ubuntu desktop and you can start using it.

Starting Visual Studio Code
---------------------------

Now that VS Code is installed on your Ubuntu system you can launch it either from the command line by typing `code` or by clicking on the VS Code icon (`Activities -> Visual Studio Code`).

When you start VS Code for the first time, a window like the following should appear:

![](https://linuxize.com/post/how-to-install-visual-studio-code-on-ubuntu-18-04/ubuntu-virtual-studio-code_hud927c23432d944b0a32015706555d653_106484_600x0_resize_q75_box.jpg)

You can now start installing extensions and configuring VS Code according to your preferences.

Updating Visual Studio Code
---------------------------

When a new version is released you can update the Visual Studio Code package through your desktop standard Software Update tool or by running the following commands in your terminal:

```
sudo apt update
```

Conclusion
----------

You have successfully installed VS Code on your Ubuntu 18.04 machine. Your next step could be to install [Additional Components](https://code.visualstudio.com/docs/setup/additional-components) and customize your [User and Workspace Settings](https://code.visualstudio.com/docs/getstarted/settings).

To learn more about VS Code visit their official [documentation](https://code.visualstudio.com/docs/) page.

If you have any questions, please leave a comment below.