sudo apt-get remove --purge mysql* -y
sudo apt-get remove --purge mysql-server mysql-client mysql-common -y
sudo apt-get autoremove -y
sudo apt-get remove dbconfig-mysql -y
sudo rm -rf /etc/mysql /var/lib/mysql -y
sudo find / -iname 'mysql*' -exec rm -rf {} \; -y
