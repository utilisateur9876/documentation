Shadow vient de passer son application ubuntu en stable

Malheureusement (une fois de plus, merci Nvidia ;-)les heureux propriétaires de carte Nvidia Optimus sont à la peine

Paye ta galère ===> beaucoup de temps: je partage la solution

Ma config: ROG G46VW - GTX 660M

1.
Installation "fraiche" d'ubuntu 18.04 et ensuite mise à jour total du système

sudo apt-get update
sudo apt-get dist-upgrade
sudo reboot
2.
Installation des drivers récents de la carte graphique via un dépôt communautaire qu'on nous propose quand on installe lutris, ce qui va nous apporter le paquet vdpau-va-driver absent pour l'instant dans les dépôts officiel d'Ubuntu 18.04, ce paquet est utile plus bas dans le post

https://github.com/lutris/lutris/wiki/I … ng-drivers

sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt update
sudo apt install nvidia-driver-418

3.
Installation de l'application
On télécharge le .deb dans son espace client:
https://account.shadow.tech/apps

sudo dpkg -i le_paquet_shadow_téléchargé.deb
sudo apt-get -f install
(on peut noter ici que la Beta du moment prépare une AppImage pour la suite)

4.
Paramétrage de la carte graphique grâce au retour de la communauté

sudo apt vainfo
vainfo 
Ceci permet de vérifier que la carte est bien paramétré
Source:
https://nicolasguilloux.github.io/blade-shadow-beta/
Le reste est issue du lien que je viens de citer

sudo apt-get install vdpau-va-driver libvdpau1
Source:
https://doc.ubuntu-fr.org/vaapi

Et enfin:

wget https://gitlab.com/aar642/libva-vdpau-driver/-/jobs/205841211/artifacts/raw/vdpau-va-driver_0.7.4-6ubuntu1_amd64.deb
sudo dpkg -i vdpau-va-driver_0.7.4-6ubuntu1_amd64.deb
sudo apt-mark hold vdpau-va-driver:amd64
Source:
https://gitlab.com/aar642/shadowos-boot#support
https://gitlab.com/aar642/libva-vdpau-driver


BUG connus:
Au premier lancement de shadow (et non de l'application), plantage
Aux lancement suivant du shadow, tout tourne impeccable ...

J'ai eu un ou deux plantages ou j'ai utilisé les "touches magiques ubuntu" qui agissent directement sur le noyau:

Ici pour un reboot de l'ordi:
Alt SysRq b ou SysRq est en général la touche "impression écran"

Source:
https://doc.ubuntu-fr.org/touches_magiques

@+ tard