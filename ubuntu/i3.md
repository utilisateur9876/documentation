1.  PRE-INSTALL

    First make sure that ubuntu (and drivers) are fully installed. If it crashes at start-up, try disableing dedicated GPUs and installing those drivers afterwards

2.  BASICS

    Install i3-wm using `sudo apt-get install i3` Also install i3llock, i3status, nitrogen (bacground wallpaper), Also: `sudo apt-get install lxappearance gtk-chtheme qt4-qtconfig` to de-uglify gnome Start with lxappearance and choose a theme; then choose it in gtk-chtheme

3.  PERMISSIONS

    To enable installing from the gnome software center you need the permission elevation. For this install policykit-desktop-privilege, policykit-1-gnome, gnome-setting,daemon. Append the following lines to the i3 config file (~/.config/i3/conf): `exec /usr/libexec/gnome-settings-daemon` `exec --no-startup-id /usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1 &`

4.  SHELL

    Install zsh using `śudo apt-get install zsh` Install oh-my-zsh from the repository command found here: <https://github.com/robbyrussell/oh-my-zsh> Choose a theme, use the gnome-integrated terminal (or something like that) Download and Update Fonts with something like this <https://gist.github.com/shaykalyan/cd276a7d812dd393caf1>