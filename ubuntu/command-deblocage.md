Si problème après installation de driver pour NVIDIA et que la commande : apt --fix-broken install ne fonctionne pas en plus d'erreur alors il faut faire :

```
LC_MESSAGES=C dpkg-divert --list '*nvidia-340*' | sed -nre 's/^diversion of (.*) to .*/\1/p' | xargs -rd'\n' -n1 -- sudo dpkg-divert --remove


sudo apt --fix-broken install
```

Si probléme de permission avec sudo et avec un message parlant de /usr/bin/sudo
Il faut redemarer en recovery, utilise le mode admin en terminal et faire :
```
chown root:root /ur/bin/sudo
chmod u+s /user/bin/sudo
```