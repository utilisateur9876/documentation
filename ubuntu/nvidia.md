Une fois l'installation des drivers faite, on peut switcher d'écran avec la commande (reboot nécéssaire) :

```
sudo prime-select nvidia
```
ou
```
sudo prime-select intel
```

Si problème et perte affichage complet, par exmeple il reste que 600x480 alors il faut ajouter une nouvelle résolution :

```
sudo xrandr --newmode "1600x1080_60.00"  118.25  1600 1696 1856 2112  900 903 908 934 -hsync +vsync
sudo xrandr --addmode eDP-1 "1600x1080_60.00"
```


2 Ways to Install Nvidia Driver on Ubuntu 18.04 (GUI & Command Line)
====================================================================

Last Updated: August 20, 2019 [Xiao Guoan (Admin)](https://www.linuxbabe.com/author/xiao-guoan) 

[20 Comments](https://www.linuxbabe.com/ubuntu/install-nvidia-driver-ubuntu-18-04#comments)

[Ubuntu](https://www.linuxbabe.com/ubuntu)

This tutorial will be showing you 2 ways to install Nvidia graphics card driver on [Ubuntu 18.04](https://www.linuxbabe.com/tag/ubuntu-18-04). The first method uses graphical user interface (GUI); The second method is done from the command line interface (CLI). Ubuntu comes with the open source nouveau driver which is included in the Linux kernel for Nvidia cards.  However, this driver lacks 3D acceleration support. If you are a gamer or need to work with 3D graphics, then you will benefit from the better performance of the proprietary Nvidia driver.

Note: Before installing the proprietary driver, I recommend disabling secure boot if you are using UEFI firmware. If secure boot is enabled, you might encounter problems after installing Nvidia driver. This tutorial also assumes that you are running Ubuntu 18.04 with x.org display server.

How to Install Nvidia Graphics Driver on Ubuntu 18.04 From Graphical User Interface
-----------------------------------------------------------------------------------

First, go to `system settings` >` details` and check what graphics card your computer is using. By default, your integrated graphics card (Intel HD Graphics) is being used.

![ubuntu 18.04 nvidia 390 driver](https://www.linuxbabe.com/wp-content/uploads/2018/06/ubuntu-18.04-nvidia-390-driver.png)

Then open `softare & updates` program from you application menu. Click the `additional drivers` tab. You can see what driver is being used for Nvidia card (Nouveau by default) and a list of proprietary drivers.

![ubuntu 18.04 nvidia-drivers 430](https://www.linuxbabe.com/wp-content/uploads/2018/06/ubuntu-18.04-nvidia-drivers-430.png)

As you can see `nvidia-driver-430` and `nvidia-driver-390` are available for my `GeForce GTX 1080 Ti` card. So I select the first option to install `nvidia-driver-430`. Your mileage may vary. There might be some other drivers for your particular card. Click `Apply Changes` button to install the driver.

![install nvidia driver ubuntu 18.04 LTS](https://www.linuxbabe.com/wp-content/uploads/2018/06/install-nvidia-driver-ubuntu-18.04-LTS.png)

After it's installed, reboot your computer for the change to take effect. After that, go to `system settings` > `details`, you will see Ubuntu is using Nvidia graphics card.

![nvidia driver ubuntu 18](https://www.linuxbabe.com/wp-content/uploads/2018/06/nvidia-driver-ubuntu-18.png)

If you want Ubuntu to use Intel graphics card, open `Nvidia X Server Settings` from application menu. Click `PRIME Profiles` tab on the left pane, and then select Intel card on the right pane.

![ubuntu 18.04 nvidia prime profile](https://www.linuxbabe.com/wp-content/uploads/2018/06/ubuntu-18.04-nvidia-prime-profile.png)

To switch back to Nvidia card, simply select NVIDIA in `PRIME Profiles`.

How to Install Nvidia Driver on Ubuntu 18.04 From the Command Line
------------------------------------------------------------------

First, open up a terminal window and check which driver is being used for Nvidia card with the following command.

sudo lshw -c display

You can also use `video` instead of `display` as the class name.

sudo lshw -c video

By default, the open source nouveau driver is being used for Nvidia card.

![nvidia driver ubuntu 18.04](https://www.linuxbabe.com/wp-content/uploads/2018/06/nvidia-driver-ubuntu-18.04.png)

Next, run the following command to list available driver for your Nvidia card from the default Ubuntu repository.

sudo ubuntu-drivers devices

![ubuntu 18.04 nvidia driver devices](https://www.linuxbabe.com/wp-content/uploads/2018/06/ubuntu-18.04-nvidia-driver-devices.png)

As you can see, there are 3 drivers available for my `GeForce GTX 1080 Ti` card. Two are proprietary (non-free) drivers, which is recommend by Ubuntu. The other is the default open-source Nouveau driver. There may be some other drivers for your Nvidia card. To install the recommended driver, run the following command.

sudo ubuntu-drivers autoinstall

![ubuntu 18.04 sudo nvidia-drivers autoinstall](https://www.linuxbabe.com/wp-content/uploads/2018/06/ubuntu-18.04-sudo-nvidia-drivers-autoinstall.png)

This will install some 32 bit packages as indicated by the `i386` suffix. If you want to install a particular driver, run the following command.

sudo apt install nvidia-driver-version-number

For example:

sudo apt install nvidia-driver-430

After the driver is installed, we need to run the following command to reboot the computer, in order to enable `nvidia-prime`, which is technology to switch between Intel and Nvidia graphics card.

sudo shutdown -r now

Now the proprietary Nvidia driver is being used.

sudo lshw -c display

![ubuntu 18.04 nvidia 390](https://www.linuxbabe.com/wp-content/uploads/2018/06/ubuntu-18.04-nvidia-390.png)

You can check which card is being used now with following command:

prime-select query

![ubuntu 18.04 nvidia prime](https://www.linuxbabe.com/wp-content/uploads/2018/06/ubuntu-18.04-nvidia-prime.png)

If you want to use Intel graphics card, run the following command:

sudo prime-select intel

To switch back to Nvidia card, run

sudo prime-select nvidia

Notice that you may need reboot for the change to take effect.

Install the Latest Version Of Nvidia Drivers via PPA
----------------------------------------------------

Update: Canonical now includes the latest version of Nvidia drivers in Ubuntu LTS repository. The information in this section is for archive purpose only. You can ignore it.

Nvidia website maintains [a page for it's latest driver for Unix/Linux platforms](https://www.nvidia.com/object/unix.html). On this page you can see the latest version of Nvidia drivers. At the time of this writing, the latest version is **430.40**. Clicking the version number will take you to a page where you can see changelogs and whether you card is supported or not.

![Unix Drivers NVIDIA archive](https://www.linuxbabe.com/wp-content/uploads/2018/06/Unix-Drivers-NVIDIA-archive.png)

The driver included in the default Ubuntu repository can be outdated but more stable. If you would like to install the latest version, you can add a PPA [maintained by Ubuntu Team](https://lists.ubuntu.com/archives/ubuntu-desktop/2015-August/004715.html).

sudo add-apt-repository ppa:graphics-drivers/ppa

Notice that you don't need to manually run `sudo apt update` after adding PPA on Ubuntu 18.04. This operation becomes automatic on Ubuntu 18.04. Now you can run:

sudo ubuntu-drivers devices

The newest version becomes the recommended driver.

![ubuntu nvidia driver ppa](https://www.linuxbabe.com/wp-content/uploads/2018/06/ubuntu-nvidia-driver-ppa.png)

Now you can run the following command to install newest version.

sudo ubuntu-drivers autoinstall

This PPA is still considered testing. Sometimes you might encounter dependency problems like below.

![ubuntu 18.04 nvidia driver ppa](https://www.linuxbabe.com/wp-content/uploads/2018/06/ubuntu-18.04-nvidia-driver-ppa.png)

If you prefer stability over bleeding-edge, you can quit installing it. But if you are willing to take risk, you can upgrade the libraries by running the following command.

sudo apt upgrade

Then re-run the install command.

sudo ubuntu-drivers autoinstall

If there are still dependency problems, you can manually install them. For example, I saw the following error message.

The following packages have unmet dependencies:
 nvidia-driver-396 : Depends: xserver-xorg-video-nvidia-396 (= 396.24.02-0ubuntu0~gpu18.04.1) but it is not going to be installed
                     Depends: libnvidia-cfg1-396 (= 396.24.02-0ubuntu0~gpu18.04.1) but it is not going to be installed

I ran this command to fix the issue.

sudo apt install libnvidia-cfg1-396 xserver-xorg-video-nvidia-396 nvidia-driver-396

Once the newest version is installed, reboot your computer.

sudo shutdown -r now

Help Files
----------

You can find the help files by running the following command.

dpkg -L nvidia-driver-390

Repalce `390` with your driver version number.

How to Uninstall the Proprietary Nvidia Driver on Ubuntu 18.04
--------------------------------------------------------------

If for some reason you don't want the proprietary driver any more, you can remove it by running the following command.

sudo apt purge nvidia-*

sudo apt autoremove

To remove Nvidia driver PPA, run:

sudo add-apt-repository --remove ppa:graphics-drivers/ppa

I hope this tutorial helped you install Nvidia driver on Ubuntu 18.04 desktop, either from GUI or from command line. As always, if you found this post useful, then [subscribe to our free newsletter](https://newsletter.linuxbabe.com/subscription/wkeY5d6pg) to get more tips and tricks. Take care.