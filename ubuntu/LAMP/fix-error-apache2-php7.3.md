Modify the .conf file
---------------------

The first thing we must do is modify the main Apache 2 configuration file. To do this, open a terminal window and issue the command:

sudo nano /etc/apache2/apache2.conf

With apache2.conf open, all you have to do is add the following to the bottom of the file:

<FilesMatch \.php$>
​SetHandler application/x-httpd-php
​</FilesMatch>

Save and close *apache2.conf*.

Enable/disable modules
----------------------

In order to get PHP to function properly, you have to disable the mpm_event module and enable the mpm_prefork and php7 modules. To do this, go back to your terminal window and issue the command:

sudo a2dismod mpm_event && sudo a2enmod mpm_prefork && sudo a2enmod php7.0

Restart Apache 2
----------------

You're ready to restart Apache 2. Because we've disabled/enabled modules, we have to do a full restart of Apache 2 (instead of a reloading of the configuration files). To restart Apache, go back to the terminal window and issue the command:

sudo service apache2 restart

You should now be able to point a browser to a PHP file and watch it execute properly, as opposed to saving to your local drive or displaying code in your browser.

That's it--Apache 2 should be functioning exactly as you need.

A simple fix
------------

I warned you this would be a simple fix. Apache 2 is a very admin-friendly web server to configure and manage. Although one would think executing PHP would be an out-of-the box feature, it's a very easy issue to resolve.