DNS on Ubuntu 18.04
===================

2018-10-25

For years it's been simple to set up DNS on a Linux machine. Just add a couple of entries to `/etc/resolv.conf` and you're done.

```
# Use Cloudflare public DNS servers.
nameserver 1.1.1.1
nameserver 1.0.0.1
```

But things change and now it's not that simple. If you now edit `/etc/resolv.conf` on Ubuntu you'll find that the edits are ephemeral. If you restart (or even hibernate) your machine then they'll be overwritten by default content.

```
nameserver 127.0.0.53
search Home
```

This is pretty simple to fix though.

1.  Install the `resolvconf` package.

    ```
    sudo apt install resolvconf
    ```

2.  Edit `/etc/resolvconf/resolv.conf.d/head` and add the following:

    ```
    # Make edits to /etc/resolvconf/resolv.conf.d/head.
    nameserver 1.1.1.1
    nameserver 1.0.0.1
    ```

3.  Restart the `resolvconf` service.

    ```
    sudo service resolvconf restart
    ```

Fix should be permanent.