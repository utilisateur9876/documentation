#!/bin/bash
# Si besoin decommenter ce qui est en dessous
#PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

##
## Script de sauvegarde pour votre serveur linux
##

##
## Configuration
##

echo "${bold} ______            _                                     _        "   
echo "${bold} | ___ \          | |                                   | |        "  
echo "${bold} | |_/ / __ _  ___| | ___   _ _ __    ___  ___ _ __   __| | ___ _ __" 
echo "${bold} | ___ \/ _` |/ __| |/ / | | | '_ \  / __|/ _ \ '_ \ / _` |/ _ \ '__|"
echo "${bold} | |_/ / (_| | (__|   <| |_| | |_) | \__ \  __/ | | | (_| |  __/ |   "
echo "${bold} \____/ \__,_|\___|_|\_\\__,_| .__/  |___/\___|_| |_|\__,_|\___|_|   "
echo "${bold}                             | |                                     "
echo "${bold}                             |_|                                     "
echo ""
echo "##########################"
echo "# Début de la sauvegarde #"
echo "##########################"
echo ""
echo "Configuration des répertoires var, home, etc, backup"
# Répertoire a sauvegarder indiquez ici les repertoires utile
REPERTOIRES="/var /home /etc /backup"
echo ""
echo "Le point de sauvegarde distant est dedibackup-dc2.online.net"
echo ""
echo "Répertoire ebula_web"
echo ""
# Parametre FTP
FTP_SERVEUR="monurl"
FTP_UTILISATEUR="utilisateur"
FTP_PASSWORD="mdp"
FTP_REPERTOIRE="/monrepdistant/"

##
## Parametrage des applications
##

# nom de la backup
REPERTOIRE_BACKUP="/tmp/backup/"
BACKUP="backup-blablabla.tar.gz"

# tar
TAR_OPTIONS="--exclude=$REPERTOIRE_BACKUP --exclude=/dev --exclude=/proc --exclude=/sys"

# date
DATE=$(date +"%Y-%m-%d")
echo "Date de la sauvegarde"
echo $DATE
echo ""
##
## Creation de la sauvegarde
##
echo "Création de la sauvegarde"
echo "_________________________"
mkdir -p ${REPERTOIRE_BACKUP}
mkdir -p /var/backup/
tar ${TAR_OPTIONS} -zcvf ${REPERTOIRE_BACKUP}/${BACKUP} $REPERTOIRES
echo "_________________________"
echo ""
##
## Envoi de la sauvegarde
##
echo "Envoi de la sauvegarde"
echo ""
ncftp -u"$FTP_UTILISATEUR" -p"$FTP_PASSWORD" $FTP_SERVEUR <<EOF
mkdir $FTP_REPERTOIRE
mkdir $FTP_REPERTOIRE/$DATE
cd $FTP_REPERTOIRE/$DATE
lcd $REPERTOIRE_BACKUP
mput *
quit
EOF
echo ""
echo "Suppresion du repertoire de la backup"
