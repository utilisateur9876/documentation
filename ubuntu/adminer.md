1.  After installation with apt package manager change into the **adminer directory**.

    ```
    cd /usr/share/adminer

    ```

    There you will find a file called *compile.php*.

2.  Run the following command and the **adminer-X.X.X.php** (X.X.X for your version) file will be created.

    ```
    sudo php compile.php

    ```

3.  Create the apache **adminer configuration file**.

    ```
    sudo echo "Alias /adminer.php /usr/share/adminer/adminer-X.X.X.php" | sudo tee /etc/apache2/conf-available/adminer.conf

    ```

4.  Now you'll need to **activate the configuration**.

    ```
    cd /etc/apache2/conf-available/
    sudo a2enconf adminer.conf

    ```

5.  **Reload** your apache webserver.

    ```
    sudo systemctl reload apache2.

    ```

6.  Test in your browser of choice (localhost/adminer.php)