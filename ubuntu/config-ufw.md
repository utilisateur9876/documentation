Uncomplicated Firewall[](https://doc.ubuntu-fr.org/ufw#uncomplicated_firewall)
==============================================================================

*Le pare-feu tout simplement.*

UFW est un nouvel outil de configuration simplifié en [ligne de commande](https://doc.ubuntu-fr.org/console "console") de [Netfilter](https://fr.wikipedia.org/wiki/Netfilter "https://fr.wikipedia.org/wiki/Netfilter"), qui donne une alternative à l'outil [iptables](https://doc.ubuntu-fr.org/iptables "iptables"). UFW devrait à terme permettre une configuration automatique du pare-feu lors de l'installation de programmes en ayant besoin.

Si vous désirez activer et configurer votre pare-feu sur un ordinateur profitant d'un environnement de bureau, sachez qu'il existe une interface graphique pour UFW : [Gufw](https://doc.ubuntu-fr.org/gufw "gufw").

Il existe également une interface spécialement adaptée pour KDE (Kubuntu) : [installer le paquet](https://doc.ubuntu-fr.org/tutoriel/comment_installer_un_paquet "tutoriel:comment_installer_un_paquet") **[ufw-kde](apt://ufw-kde "apt://ufw-kde")**.

Modifier

Installation[](https://doc.ubuntu-fr.org/ufw#installation)
----------------------------------------------------------

**Uncomplicated Firewall** est pré-installé sous Ubuntu, mais en cas de besoin, vous devrez simplement [installer le paquet](https://doc.ubuntu-fr.org/tutoriel/comment_installer_un_paquet "tutoriel:comment_installer_un_paquet") **[ufw](apt://ufw "apt://ufw")**.

Modifier

Utilisation[](https://doc.ubuntu-fr.org/ufw#utilisation)
--------------------------------------------------------

L'ordre de déclaration des règles est très important, le système utilisant une politique « premier arrivé, premier servi ». Prenez donc soin d'ajouter vos règles spécifiques avant les règles générales lorsqu'elles concernent des éléments communs.

Modifier

### Activer / Désactiver UFW[](https://doc.ubuntu-fr.org/ufw#activerdesactiver_ufw)

L'outil UFW n'est pas activé par défaut, il vous faut donc avoir les [droits administrateur](https://doc.ubuntu-fr.org/sudo "sudo") en [ligne de commande](https://doc.ubuntu-fr.org/console "console").

Vérifier le statut actuel :

sudo ufw status

État : actif ou inactif

Activer UFW : ( c'est à dire appliquer les règles définies)

sudo ufw enable

Désactiver UFW : (c'est à dire ne plus appliquer les règles définies)

sudo ufw disable

Modifier

### Afficher l'état actuel des règles[](https://doc.ubuntu-fr.org/ufw#afficher_l_etat_actuel_des_regles)

Une unique commande qui vous permettra de jeter un œil sur la totalité des instructions que vous avez indiquées à UFW :

sudo ufw status verbose

Cette commande devrait vous afficher quelque chose comme ça :

État : actif
Journalisation : on (low)
Default: deny (incoming), allow (outgoing), disabled (routed)
Nouveaux profils : skip

Vers                       Action      De
----                       ------      --
80                         DENY IN     Anywhere
443                        ALLOW IN    Anywhere
22                         ALLOW IN    Anywhere
192.168.0.2 995            ALLOW IN    Anywhere
8082/tcp                   DENY IN     10.0.0.0/8
25/tcp                     ALLOW IN    192.168.0.0/24
80 (v6)                    DENY IN     Anywhere (v6)
443 (v6)                   ALLOW IN    Anywhere (v6)
22 (v6)                    ALLOW IN    Anywhere (v6)

23/tcp                     DENY OUT    Anywhere
23/tcp (v6)                DENY OUT    Anywhere (v6)

L'argument `verbose` est optionnel, cependant il est vivement recommandé car il permet d'afficher la direction du trafic dans les règles (IN : entrant, OUT : sortant)

#### Description du contenu[](https://doc.ubuntu-fr.org/ufw#description_du_contenu)

Journalisation : on (low)

Indique que la journalisation est activée, vous pouvez retrouver toutes les interactions du pare-feu dans le fichier **/var/log/ufw.log** . Vous pouvez activer ou désactiver cette journalisation, voir [ici](https://doc.ubuntu-fr.org/ufw#activerdesactiver_la_journalisation "ufw ↵").

Default: deny (incoming), allow (outgoing), disabled (routed)

Concerne les règles par défaut de UFW, voir la rubrique [gestion des règles par défaut](https://doc.ubuntu-fr.org/ufw#gestion_des_regles_par_defaut "ufw ↵") pour son paramétrage.

Vers                       Action      De
----                       ------      --
80                         DENY IN     Anywhere
443                        ALLOW IN    Anywhere
[...]
23/tcp (v6)                DENY OUT    Anywhere (v6)

Liste toutes les règles que vous avez indiquées au pare-feu. (V6) correspond aux règles adaptées pour l'IPv6, celles qui n'ont pas cette précision sont adaptées pour l'IPv4. Pour l'édition de ces règles, voir la rubrique concernant [l'édition des règles](https://doc.ubuntu-fr.org/ufw#ajoutersupprimer_des_regles "ufw ↵").

#### Numéro de règle[](https://doc.ubuntu-fr.org/ufw#numero_de_regle)

Vous pouvez afficher les règles numérotées.

sudo ufw status numbered

Modifier

### Gestion des règles par défaut[](https://doc.ubuntu-fr.org/ufw#gestion_des_regles_par_defaut)

Lorsque UFW est activé, par défaut le trafic entrant est refusé et le trafic sortant est autorisé. C'est en général le réglage à privilégier, cependant vous pouvez tout de même modifier ces règles.

Autoriser le trafic entrant suivant les règles par défaut :

sudo ufw default allow

Refuser le trafic entrant suivant les règles par défaut :

sudo ufw default deny

Autoriser le trafic sortant suivant les règles par défaut :

sudo ufw default allow outgoing

Refuser le trafic sortant suivant les règles par défaut :

sudo ufw default deny outgoing

Modifier

### Les commandes de base[](https://doc.ubuntu-fr.org/ufw#les_commandes_de_base)

#### Activer/désactiver la journalisation[](https://doc.ubuntu-fr.org/ufw#activerdesactiver_la_journalisation)

Activer la journalisation :

sudo ufw logging on

Désactiver la journalisation :

sudo ufw logging off

#### Ajouter/supprimer des règles[](https://doc.ubuntu-fr.org/ufw#ajoutersupprimer_des_regles)

Autoriser une connexion entrante :

sudo ufw allow [règle]

Refuser une connexion entrante :

sudo ufw deny [règle]

Refuser une IP entrante :

Si vous voulez bloquer une IP sur tous vos services, il faut le faire "avant" les autorisations existantes. D'où le "insert 1" qui met ce "deny" avant tous les "allow". Dans le cas d'une série d'IP à bloquer vous pouvez utiliser à chaque entrée le "insert 1", pas besoin de spécifier dans le cas présent une autre place

sudo ufw insert 1 deny from [ip]

Refuser une connexion entrante, uniquement en TCP :

sudo ufw deny [port]/tcp

Refuser une connexion sortante :

sudo ufw deny out [règle]

Supprimer une règle :

sudo ufw delete allow "ou deny" [règle]

Supprimer simplement une règle d'après son [numéro](https://doc.ubuntu-fr.org/ufw#numero_de_regle "ufw ↵") :

sudo ufw delete [numéro]

-   [port] est à remplacer par le numéro du port désiré.

-   [règle] est à remplacer par le numéro du port ou le nom du [service](https://doc.ubuntu-fr.org/ufw#utilisation_des_services "ufw ↵") désiré.

-   [numéro] est à remplacer par le numéro de la règle désiré.

Modifier

### Règles simples[](https://doc.ubuntu-fr.org/ufw#regles_simples)

#### La syntaxe des règles[](https://doc.ubuntu-fr.org/ufw#la_syntaxe_des_regles)

Voici quelques exemples pour comprendre la syntaxe des règles de configuration.

-   Ouverture du port 53 en TCP et UDP :

    sudo ufw allow 53

-   Ouverture du port 25 en TCP uniquement :

    sudo ufw allow 25/tcp

#### Utilisation des services[](https://doc.ubuntu-fr.org/ufw#utilisation_des_services)

UFW regarde dans sa liste de services connus pour appliquer les règles standards associées à ces services (apache2, smtp, imaps, etc..). Ces règles sont automatiquement converties en ports.

Pour avoir la liste des services :

less /etc/services

Exemple : Autoriser le service SMTP :

sudo ufw allow smtp

2° exemple : Autoriser le port de Gnome-Dictionary (2628/tcp) :

sudo ufw allow out 2628/tcp

3° exemple : Autoriser le protocole pop3 sécurisé (réception du courrier de Gmail et autres messageries utilisant ce protocole sécurisé) :

sudo ufw allow out pop3s

Modifier

### Utilisation avancée[](https://doc.ubuntu-fr.org/ufw#utilisation_avancee)

#### Règles complexes[](https://doc.ubuntu-fr.org/ufw#regles_complexes)

L'écriture de règles plus complexes est également possible :

-   Refuser le protocole (*proto*) TCP à (*to*) tout le monde (*any*) sur le port (*port*) 80 :

    sudo ufw deny proto tcp to any port 80

-   Refuser à (*to*) l'adresse 192.168.0.1 de recevoir sur le port (*port*) 25 les données provenant (*from*) du [réseau de classe A](https://fr.wikipedia.org/wiki/Sous-r%C3%A9seau#R%C3%A9seaux_priv%C3%A9s "https://fr.wikipedia.org/wiki/Sous-réseau#R%C3%A9seaux_priv%C3%A9s") et utilisant le protocole (*proto*) TCP :

    sudo ufw deny proto tcp from 10.0.0.0/8 to 192.168.0.1 port 25

-   Refuser les données utilisant le protocole (*proto*) UDP provenant (*from*) de 1.2.3.4 sur le port (*port*) 514 :

    sudo ufw deny proto udp from 1.2.3.4 to any port 514

-   Refuser à l'adresse 192.168.0.5 de recevoir toutes données provenant du serveur web de la machine hébergeant le pare-feu :

    sudo ufw deny out from 192.168.0.5 to any port 80

#### Insérer une règle[](https://doc.ubuntu-fr.org/ufw#inserer_une_regle)

Vous pouvez insérer une règle à une position précise en utilisant le [numéro](https://doc.ubuntu-fr.org/ufw#numero_de_regle "ufw ↵")

sudo ufw insert NUM RULE

-   Insérer en numéro 2 une règle refusant le trafic entrant utilisant le protocole (*proto*) UDP (*to*) en direction de (*any*) toute les adresses en écoute sur votre machine sur le port (*port*) 514 en provenance (*from*) de 1.2.3.4

sudo ufw insert 2 deny proto udp to any port 514 from 1.2.3.4

#### Réinitialiser UFW[](https://doc.ubuntu-fr.org/ufw#reinitialiser_ufw)

En cas d'erreur de vos règles vous pouvez réinitialiser le pare feu comme au début de l'installation en ouvrant un terminal puis vous écrivez:

sudo ufw reset

Vous pouvez aussi forcer sans demander d'autorisation :

sudo ufw reset --force

Modifier

Configuration[](https://doc.ubuntu-fr.org/ufw#configuration)
------------------------------------------------------------

Modifier

### IPv6[](https://doc.ubuntu-fr.org/ufw#ipv6)

UFW prend en charge les adresses IPv6. Le support d'IPv6 est désormais activé par défaut, si ce n'est pas le cas, il suffit de [modifier le fichier](https://doc.ubuntu-fr.org/tutoriel/comment_editer_un_fichier "tutoriel:comment_editer_un_fichier") **/etc/default/ufw** et d'y mettre ceci :

[/etc/default/ufw](https://doc.ubuntu-fr.org/_export/code/ufw?codeblock=16 "Télécharger cet extrait")

IPV6=yes

Il ne reste plus qu'à relancer UFW :

sudo ufw reload

Si le support d'IPv6 n'était pas activé, il vous faudra sûrement alors supprimer puis recréer vos règles.

Modifier

### Ne pas autoriser le ping[](https://doc.ubuntu-fr.org/ufw#ne_pas_autoriser_le_ping)

Par défaut UFW autorise les requêtes de ping (ICMP Echo Requests). Il faut [éditer](https://doc.ubuntu-fr.org/tutoriel/comment_editer_un_fichier "tutoriel:comment_editer_un_fichier") `/etc/ufw/before.rules` et commenter en ajoutant un "**#**" à la ligne suivante :

# -A ufw-before-input -p icmp --icmp-type echo-request -j ACCEPT

Modifier

Problèmes connus[](https://doc.ubuntu-fr.org/ufw#problemes_connus)
------------------------------------------------------------------

Modifier

### Si vous obtenez **"ERROR: / is world writable[](https://doc.ubuntu-fr.org/ufw#si_vous_obtenez_erroris_world_writable)

en voulant activer Uncomplicated Firewall, ces commandes devraient régler le problème :

sudo chown root:root /
sudo chmod 0755 /

Modifier

### Si vous n'avez plus de place disque[](https://doc.ubuntu-fr.org/ufw#si_vous_n_avez_plus_de_place_disque)

Il faut penser qu'il y a un problème de refus de connexion tracé dans **/var/log**. Si non résolvable, l'une de ces commandes pourra masquer le problème:

sudo  ufw logging off
sudo  ufw logging low