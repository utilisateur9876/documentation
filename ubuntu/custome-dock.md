How to customize dock panel on Ubuntu 18.04 Bionic Beaver Linux
===============================================================

Lubos Rendek

Ubuntu 18.04

13 September 2018

Contents

-   -   [1\. Objective](https://linuxconfig.org/how-to-customize-dock-panel-on-ubuntu-18-04-bionic-beaver-linux#h1-objective)
    -   [2\. Operating System and Software Versions](https://linuxconfig.org/how-to-customize-dock-panel-on-ubuntu-18-04-bionic-beaver-linux#h2-operating-system-and-software-versions)
    -   [3\. Requirements](https://linuxconfig.org/how-to-customize-dock-panel-on-ubuntu-18-04-bionic-beaver-linux#h3-requirements)
    -   [4\. Difficulty](https://linuxconfig.org/how-to-customize-dock-panel-on-ubuntu-18-04-bionic-beaver-linux#h4-difficulty)
    -   [5\. Conventions](https://linuxconfig.org/how-to-customize-dock-panel-on-ubuntu-18-04-bionic-beaver-linux#h5-conventions)
    -   [6\. Instructions](https://linuxconfig.org/how-to-customize-dock-panel-on-ubuntu-18-04-bionic-beaver-linux#h6-instructions)

Objective
---------

The objective of this article is to provide the reader with a basic instructions on how to customize the dock panel on the default Gnome Ubuntu 18.04 Bionic Beaver desktop.

Operating System and Software Versions
--------------------------------------

-   Operating System: - Ubuntu 18.04 Bionic Beaver
-   Software: - GNOME Shell 3.28.1 or higher

Requirements
------------

Privileged access to your Ubuntu System as root or via `sudo` command is required.

Difficulty
----------

EASY

Conventions
-----------

-   # - requires given [linux commands](https://linuxconfig.org/linux-commands) to be executed with root privileges either directly as a root user or by use of `sudo` command
-   $ - requires given [linux commands](https://linuxconfig.org/linux-commands) to be executed as a regular non-privileged user

Instructions
------------

There are variety of options for Dock customization on the Ubuntu 18.04 Gnome desktop.

[![Custom Dock panel on Ubuntu 18.04 Bionic Beaver Desktop](https://linuxconfig.org/images/01-customize-dock-on-ubuntu-18.04.png)](https://linuxconfig.org/images/01-customize-dock-on-ubuntu-18.04.png "Custom Dock panel on Ubuntu 18.04 Bionic Beaver Desktop")

Custom Dock panel on Ubuntu 18.04 Bionic Beaver Desktop

* * * * *

*SUBSCRIBE TO NEWSLETTER\
Subscribe to Linux Career [NEWSLETTER](https://bit.ly/2X5D30q) and receive latest Linux news, jobs, career advice and tutorials.*

* * * * *

* * * * *

[![Using settings windows to customize dock on Ubuntu 18.04 Gnome Desktop](https://linuxconfig.org/images/02-customize-dock-on-ubuntu-18.04.png)](https://linuxconfig.org/images/02-customize-dock-on-ubuntu-18.04.png "Using settings windows to customize dock on Ubuntu 18.04 Gnome Desktop")

First we will use the provided settings window to perform some basic customization. Navigate to `Settings-->Dock` if you wish to perform some basic dock customization such as auto-hide, icon size and Dock position on the screen.

For more Dock customization options we can use `dconf-editor`. `dconf-editor` is not installed by default but you can install it by [openning up a terminal](https://linuxconfig.org/how-to-open-a-terminal-on-ubuntu-bionic-beaver-18-04-linux) and entering the following [linux command](https://linuxconfig.org/linux-commands):

$ sudo apt install dconf-tools

[![dconf-editor on Ubuntu 18.04](https://linuxconfig.org/images/03-customize-dock-on-ubuntu-18.04.png)](https://linuxconfig.org/images/03-customize-dock-on-ubuntu-18.04.png "dconf-editor on Ubuntu 18.04")

Start `dconf-editor` from your terminal or by searching the `Activities` menu.

Next, navigate to: `org->gnome->shell->extensions->dash-to-dock` schema. Feel free to browse and play with any settings available on this page to customize your Dock panel.

All Dock customization can also be accomplished from the command line. For example the following set of commands will turn your Dock panel into:

[![Unity backlit like Dock on default Ubuntu 18.04 Bionic Beaver Desktop.](https://linuxconfig.org/images/04-customize-dock-on-ubuntu-18.04.png)](https://linuxconfig.org/images/04-customize-dock-on-ubuntu-18.04.png "Unity like Dock on default Ubuntu 18.04 Bionic Beaver Desktop.")

Unity like backlit Dock panel on the default Ubuntu 18.04 Bionic Beaver Desktop.

* * * * *

* * * * *

$ gsettings set org.gnome.shell.extensions.dash-to-dock extend-height false
$ gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM
$ gsettings set org.gnome.shell.extensions.dash-to-dock transparency-mode FIXED
$ gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 64
$ gsettings set org.gnome.shell.extensions.dash-to-dock unity-backlit-items true

Do not worry if you mess some things up! In that case use `gsettings`' `reset` directive to reset any settings to their default value. For example the below command will reset the Dock icon size to default:

$ gsettings reset org.gnome.shell.extensions.dash-to-dock dash-max-icon-size